package com.lizk.study;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lizk.study.animation.FrameAnimation;
import com.lizk.study.animation.PropertyAnimationActivity;
import com.lizk.study.animation.TweenAnimation;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
    }

    private void initData() {
        Button frameAnimation = findViewById(R.id.frame_animation);
        Button tweenAnimation = findViewById(R.id.tween_animation);
        Button propertyAnimation = findViewById(R.id.property_animation);


        frameAnimation.setOnClickListener(this);
        tweenAnimation.setOnClickListener(this);
        propertyAnimation.setOnClickListener(this);

//        frameAnimation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this,FrameAnimation.class);
//                startActivity(intent);
//            }
//        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frame_animation:
                Intent intent1 = new Intent(MainActivity.this,FrameAnimation.class);
                startActivity(intent1);
                break;
            case R.id.tween_animation:
                Intent intent2 = new Intent(MainActivity.this,TweenAnimation.class);
                startActivity(intent2);
                break;
            case R.id.property_animation:
                Intent intent3 = new Intent(MainActivity.this,PropertyAnimationActivity.class);
                startActivity(intent3);
                break;
            default:return;

        }

    }
}
