package com.lizk.study.animation;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.lizk.study.R;

public class FrameAnimation extends AppCompatActivity {


    AnimationDrawable animationDrawable = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_animation);


        initData();

    }

    private void initData() {
        ImageView animationImg =  findViewById(R.id.imageView);
        animationImg.setImageResource(R.drawable.anim_frame_animation);
        animationDrawable = (AnimationDrawable)animationImg.getDrawable();

        Button startOrStopButton = findViewById(R.id.start_stop_button);

        startOrStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(animationDrawable.isRunning()){
                    Button button = (Button) v;
                    button.setText("开始");
                    animationDrawable.stop();
                }else{
                    Button button = (Button) v;
                    button.setText("停止");
                    animationDrawable.start();
                }

            }
        });

    }
}
