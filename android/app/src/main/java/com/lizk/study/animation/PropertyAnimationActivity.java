package com.lizk.study.animation;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.PathInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.lizk.study.R;

public class PropertyAnimationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "PropertyAnimationActivi";

    ImageView imageView = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_animation);
        initData();
    }

    private void initData() {
        imageView = findViewById(R.id.imageView3);
        Button buttonAlphaAnimation = findViewById(R.id.button_alpha_animation);
        Button buttonTranslateAnimation = findViewById(R.id.button_translate_animation);
        Button buttonRotateAnimation = findViewById(R.id.button_rotate_animation);
        Button buttonScaleAnimation = findViewById(R.id.button_scale_animation);
        Button buttonSetAnimation = findViewById(R.id.button_set_animation);


        buttonAlphaAnimation.setOnClickListener(this);
        buttonTranslateAnimation.setOnClickListener(this);
        buttonRotateAnimation.setOnClickListener(this);
        buttonScaleAnimation.setOnClickListener(this);
        buttonSetAnimation.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_alpha_animation:
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView,"alpha",1,0,1);
                objectAnimator.setInterpolator(
//                        new BounceInterpolator()
                        //new AnticipateInterpolator()
                        new OvershootInterpolator()
                );

                objectAnimator.setDuration(2000);
                objectAnimator.start();
                break;
            case R.id.button_translate_animation:
                ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(imageView,"translationX",500,0);
                ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(imageView,"translationY",500,0);
                objectAnimator1.setInterpolator(
//                        new BounceInterpolator()
                    //new AnticipateInterpolator()
                        new OvershootInterpolator()
                );
                objectAnimator1.setDuration(2000);
                objectAnimator2.setDuration(2000);
                objectAnimator1.start();
                objectAnimator2.start();
                break;
            case R.id.button_scale_animation:
                ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(imageView,"scaleX",1,0,1);
                objectAnimator3.setInterpolator(
//                        new BounceInterpolator()
                        //new AnticipateInterpolator()
                        new OvershootInterpolator()
                );
                objectAnimator3.setDuration(2000);
                objectAnimator3.start();
                break;
            case R.id.button_rotate_animation:
                //针对view的某个属性创建ObjectAnimator对象
                ObjectAnimator objectAnimator4 = ObjectAnimator.ofFloat(imageView,"rotation",0,90);
                //设置差值器，增加动画效果
                objectAnimator4.setInterpolator(
                        //new BounceInterpolator()
                        //new AnticipateInterpolator()
                        new OvershootInterpolator()
                );
                //设置动画持续时间
                objectAnimator4.setDuration(2000);
                //开始动画
                objectAnimator4.start();
                break;
            case R.id.button_set_animation:



                //定义两个自定义的起始值
                MyPoit begin = new MyPoit(imageView.getX(),imageView.getY());
                MyPoit end = new MyPoit(800,400);
                Log.d(TAG, "evaluate: "+begin);
                Log.d(TAG, "evaluate: "+end);


                //使用ValueAnimator 来设置动画效果，设置自定义的“估值器” ，用来返回具体设置到view中的值
//                插值器（Interpolator）决定 值 的变化模式（匀速、加速blabla）
//                估值器（TypeEvaluator）决定 值 的具体变化数值
                ValueAnimator valueAnimator = ValueAnimator.ofObject(new TypeEvaluator(){
                    @Override
                    public Object evaluate(float fraction, Object startValue, Object endValue) {

                        MyPoit begin = (MyPoit)startValue;
                        MyPoit end = (MyPoit)endValue;

                        MyPoit mod = new MyPoit(
                                begin.getX() + (end.getX()-begin.getX()) * fraction,
                                begin.getY() + (end.getY()-begin.getY()) * fraction);

                        Log.d(TAG, "evaluate: "+mod);
                        return mod;
                    }
                },begin,end,begin);


                //给动画增加监听，监听值变化的时候的处理
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        MyPoit tmp = (MyPoit)animation.getAnimatedValue();
                        //设置view的值
                        imageView.setX(tmp.getX());
                        imageView.setY(tmp.getY());
                        imageView.invalidate();
                        //刷新view
                        //imageView.requestLayout();
                    }
                });
                valueAnimator.setDuration(1000);
                valueAnimator.setInterpolator(new BounceInterpolator());
                valueAnimator.start();

                break;
            default:return;
        }
    }


    /**
     * 自定义对象，用来存储动画需要改变的值
     */
    public  static class MyPoit{
        private float x ;
        private float y ;


        public MyPoit() {
        }

        public MyPoit(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public float getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return "MyPoit{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}
