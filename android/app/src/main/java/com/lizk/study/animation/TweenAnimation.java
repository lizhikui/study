package com.lizk.study.animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.lizk.study.R;

public class TweenAnimation extends AppCompatActivity {


    ImageView imageView2 =  null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tween_animation);
        initData();
    }

    private void initData() {
        Button button = findViewById(R.id.alpha_animation);
        imageView2 = findViewById(R.id.imageView2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button)v;
                Animation animation = AnimationUtils.loadAnimation(TweenAnimation.this,R.anim.alpha_animation);
                animation.setFillAfter(true);
                imageView2.startAnimation(animation);
            }
        });
    }
}
