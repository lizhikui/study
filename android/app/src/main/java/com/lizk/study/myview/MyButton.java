package com.lizk.study.myview;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;

import com.lizk.study.R;


/**
 * 自定义的组件
 */
public class MyButton extends AppCompatButton {
    private static final String TAG = "MyButton";
    
    public MyButton(Context context) {
        super(context);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //创建对象以后，设置默认的样式·
    {
        this.setBackgroundResource(R.drawable.style_button);
    }
    @Override
    public boolean hasOnClickListeners() {
        this.setText("---------------------");
        Log.d(TAG, "hasOnClickListeners: 调用了");
        return super.hasOnClickListeners();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d(TAG, "onLayout: 调用了");
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw: 调用了");
        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG, "onMeasure: 调用了");
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
