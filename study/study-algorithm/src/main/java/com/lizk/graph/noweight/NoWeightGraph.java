package com.lizk.graph.noweight;

import java.util.Iterator;

/**
 * @author lizhikui
 * @date 2020/2/15 16:41
 */
public interface NoWeightGraph<T> extends Iterator<T> {
    /**
     * 添加连通的边
     * @param row   第一个元素的索引
     * @param col   第二个元素的索引
     */
    void addEdge(int row,int col);

    /**
     * 判断两个元素是否连同
     * @param row   第一个元素的索引
     * @param col   第二个元素的索引
     * @return      true 或者false 连通或者不连通
     */
    boolean hasEdge(int row,int col);

    /**
     * 迭代与指定元素连通的所有元素
     * @param row   指定的元素索引
     */
    void iterator(int row);


}
