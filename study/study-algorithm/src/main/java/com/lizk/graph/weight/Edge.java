package com.lizk.graph.weight;


import com.lizk.tools.base.Checks;
import lombok.Getter;
import lombok.Setter;

/**
 * @author lizhikui
 * @date 2020/2/21 14:12
 */
@Setter
@Getter
public class Edge <W extends Comparable<W>> implements Comparable<Edge<W>>{
    int from;
    int to;
    W weight;


    public Edge(int from ,int to , W weight){
        this.from = from;
        this.to = to ;
        this.weight = weight;
    }

    /**
     * 根据边相连接的其中一个节点的索引,获取另一个节点的索引值
     * @param one 其中一个节点索引
     * @return  另一个节点的索引
     */
    public int other (int one){
        Checks.checkArgument(one == from || one == to );
        return one == from ? to : from;
    }

    @Override
    public String toString() {
        return from + "-" + to +":" + weight;
    }

    @Override
    public int compareTo(Edge<W> o) {
        return weight.compareTo(o.weight);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + from;
        result = 31 * result + to;
        result = 31 * result + weight.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Edge) {
            Edge<W> edge = (Edge<W>)obj;
            return this.getFrom() == edge.getFrom()&&
                    this.to == edge.getTo() &&
                    this.weight.equals(edge.getWeight());
        }
        return false;
    }
}
