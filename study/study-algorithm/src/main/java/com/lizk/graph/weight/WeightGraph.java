package com.lizk.graph.weight;

import java.util.Iterator;

/**
 * @author lizhikui
 * @date 2020/2/15 16:41
 */
public interface WeightGraph<T> extends Iterator<T> {
    /**
     * 添加连通的边
     */
    void addEdge(T t);


    int size();

    /**
     * 迭代与指定元素连通的所有元素
     * @param row   指定的元素索引
     */
    void iterator(int row);


    boolean hasEdge(T edge);
}
