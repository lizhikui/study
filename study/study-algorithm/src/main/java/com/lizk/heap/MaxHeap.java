package com.lizk.heap;

import com.lizk.sort.SortUtils;

/**
 * 索引从1开始的最大堆
 * @author lizhikui
 * @date 2020/2/9 14:47
 */
public class MaxHeap {
    /*
        基本知识
        1.普通队列：先进先出，后进后出
        2.优先队列：出队顺序与入队顺序无关，和优先级有关
        3.二叉堆 (Binary Heap):
            1）每一个结点有两个子节点
            2）任何一个子节点都不大于父节点
            3）必须是完全二叉树，初最后一层外，其他层必须是结点的最大数
            4）最大堆，第一个结点是最大值
        4.使用数组记录二叉堆，
            1）从第一个结点开始一次从左向右，从上到下标号，就会得到以下序列
                                1
                        2               3
                   4        5        6       7
                 8   9  10    11  12  13  14    15
             2）所以可以使用数组来记录二叉树，左孩子的索引2*parentIndex，有孩子2*parentIndex+1
     */

    /**
     * 当前的存储数据的数量
     */
    private int size;
    /**
     * 存储数量的数据
     */
    private Integer [] value;

    /**
     * 构造函数，最大堆的索引从1开始
     * @param length  指定存储数据数组的长度
     */
    public MaxHeap(int length){
        this.size  = 0;
        value = new Integer[length + 1];
    }

    public MaxHeap(int [] array){
        this.heapify(array);
    }

    /**
     * 插入一个元素
     * @param item  插入的数据
     */
    public void insertValue(int item){
        //如果数组已经满了，那么直接报错
        if (size + 1 >= value.length){
            throw new RuntimeException("数据已存满");
        }
        size ++;
        value[size] = item;
        //把插入的元素自底向上调整到合适的位置
        shiftUp();
    }

    public int takeValue(){
        int result =  value[1];
        value[1] = value[size];
        value[size] = 0;
        size --;
        shiftDown(1);
        return result;
    }

    /**
     * 把完全没有顺序的完全二叉树，转换成最大堆
     * 思路：
     * 1.把每个最后的叶子结点看成是一个组织好的最大堆
     * 2.然后从最后一个非叶子结点开始，一次做shiftdown处理
     * 3.这里通过array直接构造一个堆的时间复杂度为n级别
     */
    public void heapify(int [] array){
        value = new Integer[array.length + 1];
        size = array.length;
        for (int i = 0; i < array.length; i++) {
            value[i+1] = array[i];
        }

        for (int i = size/2 ; i >=1 ; i--){
            shiftDown(i);
        }
    }

    /**
     * 把最后一个不符合规则的元素，依次向上移动，直到合适的位置。
     */
    private void shiftUp(){
        //步长为2，所以时间复杂度为logn
        for (int i = size ; i > 1 ; i=i/2){
            //当前位置与父节点比较，如果父节点小，那么交换位置，循环处理的节点转到当前节点的父节点
            if(value[i] > value[i/2]){
                SortUtils.swap(value,i,i/2);
            }
        }
    }

    /**
     * 把最上面不符合规则的元素依次向下移动，直到合适的位置
     */
    private void shiftDown(int index){
        //每次长两个步长，所以整个算法的时间复杂度为logn级别的


        for (int i = index; i * 2 <= size;) {
            int tmp = i * 2;
            if (i*2+1 <= size && value[i * 2] < value[i*2+1]){
                tmp = i * 2 + 1;
            }

            if (value[i] < value[tmp]){
                SortUtils.swap(value,i,tmp);
                i = tmp;
            }else {
                break;
            }
        }
    }

    /**
     * 计算指定索引元素所在的树的层次
     * @return
     */
    private int calcFloor(int index){
        double d = Math.log(index)/Math.log(2);
        int floor = (int)Math.ceil(d);
        return floor;
    }

    //打印树
    public void print(){
        //计算指定索引元素所在的树的层次
            /*double d = Math.log(size +1 )/Math.log(2);
            int totalFloor = (int)Math.ceil(d);*/
        //计算树一共有多少层
        int totalFloor = calcFloor(size + 1);

        int tmpCurrFloor = -1;
        for (int i = 1; i < size + 1; i++) {
            //计算当前元素所在的层次
            int currFloor = calcFloor(i + 1);

            //如果是新的层次，那么输出换行符
            if (tmpCurrFloor == -1 || tmpCurrFloor != currFloor){
                if(tmpCurrFloor != -1){
                    System.out.println("\r\n");
                }
                tmpCurrFloor = currFloor;
            }

            //输出新行前的tab
            //1.如果是新的行，那么输出行前tab
            //2.tab的数量是  2的（总行数 - 当前行）次方 -1
            if (Math.pow(2,currFloor - 1) ==  i){
                double size = Math.pow(2, totalFloor - currFloor) - 1;
                for (int j = 0 ; j < size ; j++)
                    System.out.print("\t");

            }
            //输出元素
            System.out.print(value[i]);

            //输出当前行应该输出的分隔tab数，tab的数量是  2的(总行数 - 当前行 +1)次方个
            for (int j = 0; j < Math.pow(2,totalFloor - currFloor +1); j++) {
                System.out.print("\t");
            }

        }
        System.out.println();
    }

}
