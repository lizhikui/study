package com.lizk.leetcode.containerwater;

/**
 * 11. 盛最多水的容器
 * https://leetcode-cn.com/problems/container-with-most-water/
 */
public class Client {

    public static void main(String[] args) {
        Client client = new Client();
        System.out.println(client.maxArea(new int[]{1,8,6,2,5,4,8,3,7}));
        System.out.println(client.maxArea(new int[]{6,8,6,2,5,4,8,3,7}));
        System.out.println(client.maxArea(new int[]{1,1}));

    }


    /**
     * 两个指针的方式
     * @param height
     * @return
     */
    public int maxArea(int[] height) {
        int i = 0, j = height.length - 1, result = 0;
        while(i < j){
            result = height[i] < height[j] ?
                    Math.max(result, (j - i) * height[i++]):
                    Math.max(result, (j - i) * height[j--]);
        }
        return result;
    }



    /**
     * 最简单的嵌套循环实现,计算出每个值与另外其他所有值的可能面积,然后比较
     */
    public int maxArea2(int[] height) {
        int result = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = i+1; j < height.length; j++) {
                int sum = Integer.min(height[i],height[j])*Math.abs(j-i);
                if (result<sum){
                    result = sum;
                }
            }
        }
        return result;
    }

    /**
     * 利用找距离当前值最远的大于等于当前值的索引,计算出每个值的最大面积,然后比较
     */
    public int maxArea3(int[] height) {
        int result = 0;
        int left ,right ;
        for (int i = 0; i < height.length; i++) {
            left = 0;
            right = height.length - 1;
            while (left < i){
                if (height[left] >= height[i]){
                    break;
                }
                left++;
            }
            while (right > i ){
                if (height[right] >= height[i]){
                    break;
                }
                right--;
            }
            result = Integer.max(height[i] * Integer.max(right - i,i - left),result);

        }
        return result;
    }
}
