package com.lizk.leetcode.movezeros;

import com.alibaba.fastjson.JSON;

/**
 * @author lizhikui
 * @date 2020/8/27 22:47
 */
public class Client {
    public static void main(String[] args) {
        int[] nums = new int[]{5,0,1,0,3,12,0};
        int count = 0;
        int index = 0;
        while (index < nums.length){
            if (nums[index] == 0){
                count++;
            }else{
                if (index-count != index){
                    nums[index-count] = nums[index];
                    nums[index] = 0;
                }
            }
            index++;
        }
        System.out.println(JSON.toJSONString(nums));
    }
}
