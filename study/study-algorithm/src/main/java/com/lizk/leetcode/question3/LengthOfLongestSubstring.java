package com.lizk.leetcode.question3;

/**
 * @author lizhikui
 * @date 2020/3/10 22:48
 */
public class LengthOfLongestSubstring {


    public static int lengthOfLongestSubstring(String s) {
        if (s.length()<=0){
            return 0;
        }
        if (s.length()==1){
            return 1;
        }
        int result = 1;
        int tmpResult = 1;
        char[] chars = s.toCharArray();

        int repeatIndex = 0;
        boolean isRepeat = false;

        for (int i = 1 ; i < chars.length ; i ++){

            for (int j = repeatIndex ; j < i ; j ++){
                if (chars[i] == chars[j]){
                    System.out.println(j+1);
                    repeatIndex = j+1;
                    isRepeat = true;
                    break;
                }
            }
            if (isRepeat){
                i = repeatIndex;
                System.out.println(i);
                if(tmpResult > result){
                    result = tmpResult;
                }
                tmpResult = 1;
                isRepeat = false;
            }else{
                tmpResult ++;
            }
        }

        if(tmpResult > result){
            result = tmpResult;
        }
        return result;
    }


    public static int lengthOfLongestSubstring2(String s) {
        if (s.length()<=0){
            return 0;
        }
        if (s.length()==1){
            return 1;
        }
        int result = 1;
        char[] chars = s.toCharArray();

        int begin = 0;

        for (int i = 0 ; i < chars.length ; i ++){
            for ( int j  = begin; j < i; j++) {
                if (chars[i] == chars[j]){
                    begin = j+1;
                    break;
                }
            }
            if(i - begin +1 > result){
                result = i - begin +1;
            }

        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2(""));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("a"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("aab"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("ab"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("abc"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("abac"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("cbcbdefg"));
        System.out.println(LengthOfLongestSubstring.lengthOfLongestSubstring2("pwwkew"));
    }

}
