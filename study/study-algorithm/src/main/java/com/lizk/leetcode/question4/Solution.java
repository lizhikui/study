package com.lizk.leetcode.question4;

/**
 * @author lizhikui
 * @date 2020/4/10 18:26
 */
public class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int [] n3 = new int [nums1.length + nums2.length];
        int n1I = 0;
        int n2I = 0;
        int n3I = 0;
        while (true){
            if (getValueFromArray(nums1,n1I) < getValueFromArray(nums2,n2I)){
                n3[n3I] = getValueFromArray(nums1,n1I);
                n1I++;
            }else{
                n3[n3I] = getValueFromArray(nums2,n2I);
                n2I++;
            }
            n3I ++;
            if (n3I > n3.length/2){
                break;
            }

        }
        if (n3.length%2==0){
            return (n3[n3.length/2] + n3[n3.length/2 -1 ]) /2.0;
        }else{
            return n3[n3.length/2];
        }
    }

    int getValueFromArray(int[] nums1,int index){
        if (nums1 == null || nums1.length == 0 || index >= nums1.length){
            return Integer.MAX_VALUE;
        }
        return nums1[index];
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.findMedianSortedArrays(new int[]{},new int[] {1}));
        System.out.println(solution.findMedianSortedArrays(new int[]{3},new int[] {-2,-1}));
        System.out.println(solution.findMedianSortedArrays(new int[]{1,2},new int[] {3,4}));
    }
}
