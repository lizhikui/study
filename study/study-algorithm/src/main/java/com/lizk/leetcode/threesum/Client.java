package com.lizk.leetcode.threesum;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 15. 三数之和
 * https://leetcode-cn.com/problems/3sum/
 * 给定数组 nums = [-1, 0, 1, 2, -1, -4]，
 *
 * 满足要求的三元组集合为：
 * [
 *   [-1, 0, 1],
 *   [-1, -1, 2]
 * ]
 *
 */
public class Client {
    public static void main(String[] args) {
        Client client = new Client();
       // int [] nums2 = new int []{-3,-2-1,0,1,1,3};
        int [] nums2 = new int []{-1,0,1,2,-1,-4};
        System.out.println(JSON.toJSONString(client.threeSum(nums2)));
        System.out.println(JSON.toJSONString(client.threeSum2(nums2)));
    }

    public List<List<Integer>> threeSum2(int[] nums) {
        if (nums == null || nums.length <= 2) {
            return Collections.emptyList();
        }
        Arrays.sort(nums);
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> item;

        int left = 0;
        int right ;
        int mid ;

        while (left <= nums.length -3){
            if (left >= 1 &&  nums[left] == nums[left - 1 ]) {
                left ++;
                continue;
            }
            mid = left + 1;
            right = nums.length -1;
            while (mid < right){
                if (mid > left + 1 &&  nums[mid] == nums[mid - 1 ]) {
                    mid ++;
                    continue;
                }
                if (nums[left] + nums[mid] + nums[right] < 0){
                    mid ++;
                }else if(nums[left] + nums[mid] + nums[right] > 0){
                    right--;
                }else {
                    item = new ArrayList<>();
                    item.add(nums[left]);
                    item.add(nums[mid]);
                    item.add(nums[right]);
                    list.add(item);
                    right --;
                    mid ++;
                }
            }
            left ++;
        }
        return list;
    }


    /**
     * 排序之后用双指针实现
     */
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length <= 2) {
            return Collections.emptyList();
        }
        // sort
        Arrays.sort(nums);
        List<List<Integer>> list = new LinkedList<>();
        // first loop
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] > 0) break;
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int right = nums.length - 1;
            // second loop to judge sum of three numbers
            for (int left = i + 1; left < nums.length; left++) {
                if (left > i + 1 && nums[left] == nums[left - 1]) continue;
                while (left < right && nums[i] + nums[left] + nums[right] > 0) {
                    right--;
                }
                if (left == right) break;
                if (nums[i] + nums[left] + nums[right] == 0) {
                    list.add(Arrays.asList(nums[i], nums[left], nums[right]));
                }
            }
        }
        return list;
    }
}
