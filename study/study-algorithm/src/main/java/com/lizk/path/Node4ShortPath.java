package com.lizk.path;

import com.lizk.graph.weight.Edge;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author lizhikui
 * @date 2020/3/1 11:07
 */
@Data
@AllArgsConstructor
public class Node4ShortPath {
    private Edge<Integer> edge;
    private boolean isVisit;
    private Integer minDistance;
    private Integer nodeIndex;

}
