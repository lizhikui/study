package com.lizk.sort;

import java.util.Arrays;

/**
 * @author lizhikui
 * @date 2020/2/7 16:00
 */
public class JavaInnerSort {
    public static Integer[] sort(Integer [] arrays){
        Arrays.sort(arrays);
        return arrays;
    }
}
