package com.lizk.sort;

/**
 * @author lizhikui
 * @date 2020/2/6 12:15
 */
public class SelectSort {
    /**
     * 根据选择排序的逻辑实现的现在排序
     *
     * 选择排序的逻辑
     * 1.从待排序的所有数组中，找到最小的一个元素
     * 2.把最小的元素和数组开头的元素交换位置
     * 3.待排序的数组为减去开头元素的所有元素，然后重复步骤1,2
     */
    public static Integer[] sort(Integer [] arrays) {
        for (int i = 0 ; i < arrays.length ; i++){
            //存放最小值得索引，默认定义为第一个元素的位置
            int minValueIndex = i;
            //从右移一个位置的元素开始，把每个元素与前一个元素比较
            for (int j = i+1 ; j < arrays.length ; j ++){
                if (arrays[j] < arrays[minValueIndex]){
                    //如果右侧的元素较小，那么更新最小值的索引
                    minValueIndex = j;
                }
            }
            SortUtils.swap(arrays,i,minValueIndex);
        }
        return arrays;
    }
}
