package com.lizk.graph.noweight;

import com.lizk.graph.noweight.AdjacencyMatrix;
import com.lizk.graph.noweight.AdjacencyList;
import com.lizk.graph.noweight.NoWeightGraph;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * @author lizhikui
 * @date 2020/2/15 21:17
 */
public class NoWeightGraphTest {


    public static void printGraph(int n, NoWeightGraph graph) {
        for (int i = 0; i < n; i++) {
            System.out.print(i + "：");
            graph.iterator(i);
            while (graph.hasNext()) {
                System.out.print(graph.next() + " ");
            }
            System.out.println();
        }
    }

    public static void addGraph(int n, NoWeightGraph am) {
        Random r = new Random();
        for (int i = 0; i < 20; i++) {
            int a = r.nextInt(n);
            int b = r.nextInt(n);
            am.addEdge(a,b);
        }
    }

    public static void readGraph(String fileName, NoWeightGraph graph){


        try (BufferedReader br = new BufferedReader(new InputStreamReader(NoWeightGraph.class.getResourceAsStream(fileName)))){
            String tmpLine = null;
            int index = 1;
            while ((tmpLine = br.readLine()) != null){
                if(index == 1){
                    index ++;
                }else {
                    String[] split = tmpLine.split("-");
                    if (split.length >= 2){
                        graph.addEdge(Integer.parseInt(split[0].trim()),Integer.parseInt(split[1].trim()));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test(){
        int n = 3;
        //AdjacencyMatrix am = new AdjacencyMatrix(n,false);
        AdjacencyList am = new AdjacencyList(n,false);
        am.addEdge(0,1);
        am.addEdge(0,2);
        printGraph(n, am);

    }

    @Test
    public void readGraph(){
        int n = 6 ;
        AdjacencyMatrix am = new AdjacencyMatrix(n,false);
        readGraph("graph2.txt",am);
        printGraph(n,am);
    }


    @Test
    public void next() {

        int n = 20;
        int m = 20;
        AdjacencyMatrix am = new AdjacencyMatrix(n,false);
        AdjacencyList at = new AdjacencyList(n,false);


        testCompare(n,m,am);
        testCompare(n,m,at);

    }

    private void testCompare(int n , int m , NoWeightGraph graph){
        long begin = System.currentTimeMillis();
        addGraph(m,graph);
        printGraph(n,graph);
        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000.0);
    }


}
