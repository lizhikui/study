package com.lizk.graph.prim;

import com.lizk.graph.weight.AdjacencyMatrix;
import com.lizk.graph.weight.Edge;
import com.lizk.graph.weight.WeightGraphTest;
import org.junit.Test;

/**
 * @author lizhikui
 * @date 2020/2/23 16:39
 */
public class LazyPrimTest {

    @Test
    public void lazyPrim() {

        int n = 4;
        AdjacencyMatrix am  = new AdjacencyMatrix(n,false);
        am.addEdge(new Edge<>(0,1,4));
        am.addEdge(new Edge<>(0,2,5));
        am.addEdge(new Edge<>(0,3,3));
        am.addEdge(new Edge<>(1,3,2));
        am.addEdge(new Edge<>(2,3,4));

        WeightGraphTest.printGraph(n,am);

        LazyPrim lazyPrim = new LazyPrim();
        lazyPrim.lazyPrim(n,am);
    }

    @Test
    public void test2(){
        String fileName = "graph2.txt";
        int n = 8 ;
        AdjacencyMatrix am = new AdjacencyMatrix(n,false);
        WeightGraphTest.readGraph(fileName,am);

        LazyPrim lazyPrim = new LazyPrim();
        lazyPrim.lazyPrim(n,am);

    }
}
