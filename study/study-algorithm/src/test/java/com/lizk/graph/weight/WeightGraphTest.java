package com.lizk.graph.weight;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * @author lizhikui
 * @date 2020/2/21 15:11
 */
public class WeightGraphTest {

    public static void addGraph(int n, WeightGraph<Edge<Integer>> am) {
        Random r = new Random();
        for (int i = 0; i < 20; i++) {
            int a = r.nextInt(n);
            int b = r.nextInt(n);
            int w = r.nextInt(n);
            am.addEdge(new Edge<>(a,b,w));
        }
    }

    public static  <T extends  Comparable<T>> void printGraph(int n, WeightGraph<Edge<T>> graph) {
        for (int i = 0; i < n; i++) {
            System.out.print(i + "：");
            graph.iterator(i);
            while (graph.hasNext()) {
                System.out.print(graph.next() + " ");
            }
            System.out.println();
        }
    }

    public static void readGraph(String fileName, WeightGraph<Edge<Integer>> graph){

        try (BufferedReader br = new BufferedReader(new InputStreamReader(WeightGraph.class.getResourceAsStream(fileName)))){
            String tmpLine = null;
            int index = 1;
            while ((tmpLine = br.readLine()) != null){
                if(index == 1){
                    index ++;
                }else {
                    if (tmpLine.startsWith("#")){
                        continue;
                    }
                    String[] split = tmpLine.split(":");
                    if (split.length >= 3){
                        graph.addEdge(new Edge<Integer>(Integer.parseInt(split[0].trim()),Integer.parseInt(split[1].trim()),Integer.parseInt(split[2].trim())));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test() {
        int n = 3 ;
        AdjacencyMatrix am = new AdjacencyMatrix(20,false);
        am.addEdge(new Edge<>(0,1,2));
        am.addEdge(new Edge<>(1,2,2));

        printGraph(n,am);

        AdjacencyList al = new AdjacencyList(20,false);
        al.addEdge(new Edge<>(0,1,2));
        al.addEdge(new Edge<>(1,2,2));
        System.out.println(al.hasEdge(new Edge<>(0,1,2)));
        printGraph(n,am);
    }

    @Test
    public void testRead(){
        String fileName = "graph2.txt";
        int n = 8 ;
        AdjacencyMatrix am = new AdjacencyMatrix(n,false);
        readGraph(fileName,am);


        printGraph(n,am);
    }

}
