package com.lizk.heap;

import org.junit.Test;

/**
 * @author lizhikui
 * @date 2020/2/9 17:27
 */
public class MaxHeapFromZeroTest {

    @Test
    public void insertValue() {
    }

    @Test
    public void takeValue() {
    }

    @Test
    public void heapify() {
        MaxHeapFromZero heap = new MaxHeapFromZero(new Integer[]{1,2,3,4,5,6,7,8,9});

        heap.takeValue();
        heap.takeValue();
        heap.print();
        heap.insertValue(12);
        heap.insertValue(14);


        //heap.takeValue();
        //heap.takeValue();
        heap.print();
    }
}
