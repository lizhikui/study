package com.lizk.heap;

import org.junit.Test;

/**
 * @author lizhikui
 * @date 2020/2/9 12:00
 */
public class MaxHeapTest {

    @Test
    public void testInsert() {
        MaxHeap binaryHeap = new MaxHeap(19);
        binaryHeap.insertValue(2);
        binaryHeap.insertValue(1);
        binaryHeap.insertValue(5);
        binaryHeap.insertValue(1);
        binaryHeap.insertValue(2);
        binaryHeap.insertValue(3);
        binaryHeap.insertValue(1);
        binaryHeap.insertValue(4);
        binaryHeap.insertValue(5);
        binaryHeap.print();
        for (int i = 0; i < 4; i++) {
            System.out.println(binaryHeap.takeValue());
        }
        binaryHeap.print();
    }

    @Test
    public void heapify() {
        int [] array = new int[]{1,2,3,4,5,6,7,8};
        MaxHeap maxHeap =new MaxHeap(1);
        maxHeap.heapify(array);
        maxHeap.print();
    }
}
