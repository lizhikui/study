package com.lizk.heap;

import com.lizk.graph.weight.Edge;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/23 13:15
 */
public class MinHeapTest {

    @Test
    public void test() {
        MinHeap minHeap = new MinHeap(new Integer []{2,7,4,3,8,4,2,1});

        minHeap.print();



        MinHeap minHeap2 = new MinHeap(100);
        minHeap2.insertValue(2);
        minHeap2.insertValue(7);
        minHeap2.insertValue(4);
        minHeap2.insertValue(3);
        minHeap2.insertValue(8);
        minHeap2.insertValue(4);
        minHeap2.insertValue(2);
        minHeap2.insertValue(1);
        minHeap2.print();


        MinHeap<Edge<Integer>> minHeap3 = new MinHeap<>(100);

        minHeap3.insertValue(new Edge<>(1,1,5));
        minHeap3.insertValue(new Edge<>(1,1,2));
        minHeap3.insertValue(new Edge<>(1,1,8));
        minHeap3.insertValue(new Edge<>(1,1,3));
        minHeap3.insertValue(new Edge<>(1,1,1));

        minHeap3.print();
        minHeap3.takeValue();
        minHeap3.print();
        minHeap3.takeValue();
        minHeap3.print();
        minHeap3.takeValue();
        minHeap3.print();

    }
}
