package com.lizk.path;

import com.lizk.graph.weight.AdjacencyList;
import com.lizk.graph.weight.Edge;
import com.lizk.graph.weight.WeightGraph;
import com.lizk.graph.weight.WeightGraphTest;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/3/1 13:18
 */
public class BellmanFordTest {

    @Test
    public void bellmanFord() {
        WeightGraph<Edge<Integer>> graph = new AdjacencyList(5,true);
        WeightGraphTest.readGraph("graph3.txt",graph);
        BellmanFord bellmanFord = new BellmanFord();
        bellmanFord.bellmanFord(graph,0);
        bellmanFord.printPath(4);
    }
}
