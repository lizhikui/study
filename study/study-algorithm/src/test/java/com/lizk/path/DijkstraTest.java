package com.lizk.path;

import com.lizk.graph.weight.AdjacencyList;
import com.lizk.graph.weight.Edge;
import com.lizk.graph.weight.WeightGraph;
import com.lizk.graph.weight.WeightGraphTest;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/3/1 11:59
 */
public class DijkstraTest {

    @Test
    public void disjkstra() {
        WeightGraph<Edge<Integer>> graph = new AdjacencyList(5,false);
        WeightGraphTest.readGraph("graph1.txt",graph);
        Dijkstra dijkstra = new Dijkstra();
        dijkstra.disjkstra(graph,0);
        dijkstra.printPath(4);
    }
}
