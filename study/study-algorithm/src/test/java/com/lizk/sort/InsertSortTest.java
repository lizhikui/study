package com.lizk.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/6 17:06
 */
public class InsertSortTest {

    @Test
    public void sort() {

        Integer [] array1 = SortUtils.generateRandomArray(50000, 300000);
        Integer [] array2 = Arrays.copyOf(array1,array1.length);
        Integer [] array3 = Arrays.copyOf(array1,array1.length);
        SortUtils.testSort("选择排序",SelectSort::sort,array1);
        //SortUtils.print(array1);

        SortUtils.testSort("插入排序",InsertSort::sort,array2);


        SortUtils.testSort("插入排序",InsertSort::sort2,array3);
        //SortUtils.print(array2);
    }
}
