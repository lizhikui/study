package com.lizk.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/9 14:53
 */
public class MaxHeapSortTest {

    @Test
    public void sort() {

        Integer [] array1 = SortUtils.generateRandomArray(100000, 100000);
        Integer [] array2 = Arrays.copyOf(array1,array1.length);
        Integer [] array3 = Arrays.copyOf(array1,array1.length);

        SortUtils.testSort("堆排序",MaxHeapSort::sort,array1);
        SortUtils.testSort("原地堆排序",MaxHeapSort::sort2,array2);

        SortUtils.testSort("快速排序",QuickSort::sort,array3);
    }
}
