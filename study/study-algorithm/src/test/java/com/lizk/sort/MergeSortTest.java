package com.lizk.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/7 14:21
 */
public class MergeSortTest {

    @Test
    public void sort() {
        Integer [] array1 = SortUtils.generateRandomArray(50, 50000);
        Integer [] array2 = Arrays.copyOf(array1,array1.length);
        Integer [] array3 = Arrays.copyOf(array1,array1.length);
        Integer [] array4 = Arrays.copyOf(array1,array1.length);
        Integer [] array5 = Arrays.copyOf(array1,array1.length);
        Integer [] result1 = SortUtils.testSort("归并排序", MergeSort::sort, array1);
        Integer [] result2 = SortUtils.testSort("插入排序", InsertSort::sort, array2);
        Integer [] result3 = SortUtils.testSort("选择排序", SelectSort::sort, array3);
        Integer [] result4 = SortUtils.testSort("java排序", JavaInnerSort::sort, array4);
        Integer [] result5 = SortUtils.testSort("快速排序", QuickSort::sort, array5);
        SortUtils.print(result5);


        array1 = SortUtils.generateOrderArray(100000, 20);
        array2 = Arrays.copyOf(array1,array1.length);
        array3 = Arrays.copyOf(array1,array1.length);
        array4 = Arrays.copyOf(array1,array1.length);

        result1 = SortUtils.testSort("归并排序", MergeSort::sort, array1);
        result2 = SortUtils.testSort("插入排序", InsertSort::sort, array2);
        result3 = SortUtils.testSort("选择排序", SelectSort::sort, array3);
        result4 = SortUtils.testSort("java排序", JavaInnerSort::sort, array4);
        result5 = SortUtils.testSort("快速排序", QuickSort::sort, array5);

        /*SortUtils.print(result1);
        SortUtils.print(result2);
        SortUtils.print(result3);*/
    }
}
