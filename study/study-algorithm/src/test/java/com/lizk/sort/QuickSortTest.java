package com.lizk.sort;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/7 18:22
 */
public class QuickSortTest {

    @Test
    public void sort() {
        Integer [] array1 = null;
        Integer [] array2 = null;
        Integer [] array3 = null;
        Integer [] array4 = null;
        Integer [] array5 = null;
        Integer [] array6 = null;
        Integer [] array7 = null;
        Integer [] result1 = null;
        Integer [] result2 = null;
        Integer [] result3 = null;
        Integer [] result4 = null;
        Integer [] result5 = null;
        Integer [] result6 = null;
        Integer [] result7 = null;

        System.out.println("============普通随机数排序===============");
        array1 = SortUtils.generateRandomArray(50000, 50000);
        array2 = Arrays.copyOf(array1,array1.length);
        array3 = Arrays.copyOf(array1,array1.length);
        array4 = Arrays.copyOf(array1,array1.length);
        array5 = Arrays.copyOf(array1,array1.length);
        array6 = Arrays.copyOf(array1,array1.length);
        array7 = Arrays.copyOf(array1,array1.length);
        result1 = SortUtils.testSort("归并排序", MergeSort::sort, array1);
        result2 = SortUtils.testSort("插入排序", InsertSort::sort, array2);
        result3 = SortUtils.testSort("选择排序", SelectSort::sort, array3);
        result4 = SortUtils.testSort("java排序", JavaInnerSort::sort, array4);
        result5 = SortUtils.testSort("快速排序", QuickSort::sort, array5);
        result6 = SortUtils.testSort("优化1快速排序", QuickSort::sort2, array6);
        result7 = SortUtils.testSort("优化2快速排序", QuickSort::sort3, array7);
        //SortUtils.print(result7);


        System.out.println("============几乎有序随机数排序===============");
        array1 = SortUtils.generateOrderArray(50000, 20);
        array2 = Arrays.copyOf(array1,array1.length);
        array3 = Arrays.copyOf(array1,array1.length);
        array4 = Arrays.copyOf(array1,array1.length);
        array5 = Arrays.copyOf(array1,array1.length);
        array6 = Arrays.copyOf(array1,array1.length);
        array7 = Arrays.copyOf(array1,array1.length);

        result1 = SortUtils.testSort("归并排序", MergeSort::sort, array1);
        result2 = SortUtils.testSort("插入排序", InsertSort::sort, array2);
        result3 = SortUtils.testSort("选择排序", SelectSort::sort, array3);
        result4 = SortUtils.testSort("java排序", JavaInnerSort::sort, array4);
        result5 = SortUtils.testSort("快速排序", QuickSort::sort, array5);
        result6 = SortUtils.testSort("优化1快速排序", QuickSort::sort2, array6);
        result7 = SortUtils.testSort("优化2快速排序", QuickSort::sort3, array7);

        System.out.println("============大量重复随机数排序===============");
        array1 = SortUtils.generateRandomArray(50, 3);
        array2 = Arrays.copyOf(array1,array1.length);
        array3 = Arrays.copyOf(array1,array1.length);
        array4 = Arrays.copyOf(array1,array1.length);
        array5 = Arrays.copyOf(array1,array1.length);
        array6 = Arrays.copyOf(array1,array1.length);
        array6 = Arrays.copyOf(array1,array1.length);
        array7 = Arrays.copyOf(array1,array1.length);

        result1 = SortUtils.testSort("归并排序", MergeSort::sort, array1);
        result2 = SortUtils.testSort("插入排序", InsertSort::sort, array2);
        result3 = SortUtils.testSort("选择排序", SelectSort::sort, array3);
        result4 = SortUtils.testSort("java排序", JavaInnerSort::sort, array4);
        result5 = SortUtils.testSort("快速排序", QuickSort::sort, array5);
        result6 = SortUtils.testSort("优化1快速排序", QuickSort::sort2, array6);
        result7 = SortUtils.testSort("优化2快速排序", QuickSort::sort3, array7);

        SortUtils.print(result7);
        /*SortUtils.print(result2);
        SortUtils.print(result3);*/
    }
}
