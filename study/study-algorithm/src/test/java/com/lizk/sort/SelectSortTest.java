package com.lizk.sort;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/6 15:08
 */
public class SelectSortTest {

    @Test
    public void sort() {
        Integer[] array = SortUtils.generateRandomArray(100000, 10000);
        SortUtils.testSort("选择排序",SelectSort::sort,array);
    }
}
