package com.lizk.sort;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/6 14:18
 */
public class SortUtilsTest {

    @Test
    public void generateRandomArray() {
        Integer[] ints = SortUtils.generateRandomArray(100,1000);
        SortUtils.print(ints);
    }


    @Test
    public void generateOrderArray() {
        Integer[] ints = SortUtils.generateOrderArray(10,2);
        SortUtils.print(ints);
    }
}
