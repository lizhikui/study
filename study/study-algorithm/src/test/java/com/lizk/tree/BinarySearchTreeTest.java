package com.lizk.tree;

import org.junit.Test;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lizhikui
 * @date 2020/2/10 14:21
 */
public class BinarySearchTreeTest {


    public <K extends Comparable<K>,V> BinarySearchTree<K,V> createTree(Map<K,V> map){
        BinarySearchTree<K,V> bst = new BinarySearchTree<>();

        for (Map.Entry<K, V> entry : map.entrySet()){
            bst.insert(new BinarySearchTree.Node<>(entry.getKey(),entry.getValue()));
        }
        return bst;
    }

    @Test
    public void insert() {
        BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<>();
        BinarySearchTree.Node<Integer, Integer> node = new BinarySearchTree.Node<>(2, 3);
        bst.insert(node);
        node = new BinarySearchTree.Node<>(4, 2);
        bst.insert(node);
        node = new BinarySearchTree.Node<>(1, 2);
        bst.insert(node);

        node = new BinarySearchTree.Node<>(10, 2);
        bst.insert(node);

        node = new BinarySearchTree.Node<>(12, 2);
        bst.insert(node);
    }

    @Test
    public void insert2() {
        BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<>();
        BinarySearchTree.Node<Integer, Integer> node = new BinarySearchTree.Node<>(2, 3);
        bst.insert(node);
        node = new BinarySearchTree.Node<>(4, 2);
        bst.insert(node);
        node = new BinarySearchTree.Node<>(1, 2);
        bst.insert(node);

        node = new BinarySearchTree.Node<>(10, 2);
        bst.insert(node);

        node = new BinarySearchTree.Node<>(12, 2);
        bst.insert(node);
    }

    @Test
    public void contain() {
        BinarySearchTree<String, Integer> bst = new BinarySearchTree<>();
        BinarySearchTree.Node<String, Integer> node = new BinarySearchTree.Node<>("a", 1);
        bst.insert(node);
        node = new BinarySearchTree.Node<>("c", 1);
        bst.insert(node);
        node = new BinarySearchTree.Node<>("z", 1);
        bst.insert(node);

        node = new BinarySearchTree.Node<>("b", 1);
        bst.insert(node);

        node = new BinarySearchTree.Node<>("f", 1);
        bst.insert(node);

        System.out.println(bst.contain("b"));
    }

    /**
     * 是一个二叉搜索树的应用
     * 在一个文件中查看某个单词出现的次数
     * 使用二叉搜索树来存储单词出现的次数，来加快统计
     */
    @Test
    public void readBible() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(BinarySearchTreeTest.class.getResourceAsStream("bible.txt")));
        String line = null;
        BinarySearchTree<String, Integer> bst = new BinarySearchTree<>();
        int total = 0;
        while ((line = br.readLine()) != null) {
            String[] words = line.split("\\b");
            for (String word : words) {
                word = word.trim().toLowerCase();
                BinarySearchTree.Node<String, Integer> node = bst.search(word);
                if (node == null){
                    bst.insert(new BinarySearchTree.Node<>(word,1));
                }else{
                    node.value = node.value + 1;
                    bst.insert(node);
                }
            }
        }

        System.out.println("total size:"+total);
        System.out.println("bst size:"+bst.size);
        System.out.println("God times:"+bst.search("god").value);
    }


    @Test
    public void Traverse(){
        Map<Integer,Integer> map = new LinkedHashMap<>();
        map.put(4,4);
        map.put(6,6);
        map.put(2,2);
        map.put(1,1);
        map.put(3,3);
        map.put(5,5);
        BinarySearchTree<Integer, Integer> tree = createTree(map);

        System.out.println("======最大值=======");
        System.out.println(tree.maxValue().value);
        System.out.println("======最小值=======");
        System.out.println(tree.minValue().value);


        System.out.println("======前序遍历=======");
        tree.preTraverse(tree.root);
        System.out.println("======中序遍历=======");
        tree.inTraverse(tree.root);
        System.out.println("======后序遍历=======");
        tree.postTraverse(tree.root);

        System.out.println("======广度优先遍历，递归实现=======");
        tree.breadthFirstTraverseByDiGUI();

        System.out.println("======广度优先遍历，循环实现=======");
        tree.breadthFirstTraverseByXunHuan();

        System.out.println("======删除元素=======");
        tree.delNode(4);

        System.out.println("======广度优先遍历，循环实现=======");
        tree.breadthFirstTraverseByXunHuan();

    }



}
