package com.lizk.union_find;

import org.junit.Test;

import java.util.Random;

/**
 * @author lizhikui
 * @date 2020/2/14 16:50
 */
public class UnionFindBaseTest {

    @Test
    public void union() {
        int length = 1000000;
        UnionFindLinkOptimizeBySize ufb = new UnionFindLinkOptimizeBySize(length);

        Random r = new Random();
        long begin = System.currentTimeMillis();
        for (int i = 0 ; i < length ; i ++){
            int a = r.nextInt(length);
            int b = r.nextInt(length);
            ufb.union(a,b);
        }
        for (int i = 0 ; i < length ; i ++){
            int a = r.nextInt(length);
            int b = r.nextInt(length);
            ufb.isConn(a,b);
        }
        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000.0);


        UnionFindLinkOptimizeByRank ufb2 = new UnionFindLinkOptimizeByRank(length);

        r = new Random();
        begin = System.currentTimeMillis();
        for (int i = 0 ; i < length ; i ++){
            int a = r.nextInt(length);
            int b = r.nextInt(length);
            ufb2.union(a,b);
        }
        for (int i = 0 ; i < length ; i ++){
            int a = r.nextInt(length);
            int b = r.nextInt(length);
            ufb2.isConn(a,b);
        }
        end = System.currentTimeMillis();
        System.out.println((end-begin)/1000.0);

    }
}
