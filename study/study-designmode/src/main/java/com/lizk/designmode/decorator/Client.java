package com.lizk.designmode.decorator;

public class Client {
	public static void main(String[] args) {
		People p = new Men();
		People h = new Hat(p);
		
		h.decorator();
	}
}	
