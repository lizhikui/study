package com.lizk.designmode.decorator;

/**
 * 装饰器模式的装饰类
 * @author lizk
 *
 */
public class Hat implements People{
	private People p ;

	public Hat(People p){
		this.p = p;
	}
	@Override
	public void decorator() {
		p.decorator();
		System.out.print("戴着小红帽");
	}
	
}
