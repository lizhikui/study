package com.lizk.designmode.decorator;


/**
 * 装饰器模式的被装饰类
 * @author lizk
 *
 */
public class Men implements People{

	@Override
	public void decorator() {
		System.out.println("男人");
	}
	
}
