package com.lizk.designmode.factory;

public class Client {
	public static void main(String[] args) {
		Operation o = new OperationAddFactory().getOperation();
		o.setNum1(1);
		o.setNum2(3);
		System.out.println(o.getResult());
	}
}
