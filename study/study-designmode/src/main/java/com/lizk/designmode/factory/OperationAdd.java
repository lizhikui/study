package com.lizk.designmode.factory;

public class OperationAdd extends Operation{
	
	
	public OperationAdd() {
	}

	public double getResult(){
		return this.getNum1() + this.getNum2();
	}
	
}
