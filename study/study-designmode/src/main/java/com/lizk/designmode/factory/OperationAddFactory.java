package com.lizk.designmode.factory;


public class OperationAddFactory implements OperationFactory{

	@Override
	public Operation getOperation() {
		return new OperationAdd();
	}
	
}
