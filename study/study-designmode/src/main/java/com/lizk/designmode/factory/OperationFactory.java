package com.lizk.designmode.factory;

public interface OperationFactory {
	public  Operation  getOperation();
}	
