package com.lizk.designmode.factory;

public class OperationSub extends Operation{
	
	
	public OperationSub() {
	}

	public double getResult(){
		return this.getNum1() - this.getNum2();
	}
	
}
