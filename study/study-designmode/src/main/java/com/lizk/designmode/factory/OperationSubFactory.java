package com.lizk.designmode.factory;


public class OperationSubFactory implements OperationFactory{

	@Override
	public Operation getOperation() {
		return new OperationSub();
	}
	
}
