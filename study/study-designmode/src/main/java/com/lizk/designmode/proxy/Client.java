package com.lizk.designmode.proxy;

public class Client {
	public static void main(String[] args) {
		Subject s = new ProxySubject();
		s.sysSomeThing();
	}
}
