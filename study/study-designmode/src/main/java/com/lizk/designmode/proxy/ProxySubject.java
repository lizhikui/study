package com.lizk.designmode.proxy;

public class ProxySubject implements Subject{

	
	Subject s ;
	ProxySubject(){
		s=new RealSubject();
	}
	
	@Override
	public void sysSomeThing() {
		s.sysSomeThing();
		System.out.println("我是代理对象");
	}

}
