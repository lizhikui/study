package com.lizk.designmode.proxy;

public class RealSubject implements Subject{

	@Override
	public void sysSomeThing() {
		System.out.println("我是真实的对象");
	}

}
