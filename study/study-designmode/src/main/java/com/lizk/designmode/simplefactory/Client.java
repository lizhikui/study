package com.lizk.designmode.simplefactory;

public class Client {
	public static void main(String[] args) {
		Operation o = OperationFactory.getOperation('-');
		o.setNum1(1);
		o.setNum2(3);
		System.out.println(o.getResult());
	}
}
