package com.lizk.designmode.simplefactory;

public class OperationFactory {
	public static Operation  getOperation(char flag){
		switch (flag) {
		case '+':
			return new OperationAdd();
		case '-':
			return new OperationSub();
		default:
			return null;
		}
	}
}	
