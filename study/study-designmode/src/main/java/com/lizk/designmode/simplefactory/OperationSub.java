package com.lizk.designmode.simplefactory;

public class OperationSub extends Operation{
	
	
	public OperationSub() {
	}

	public double getResult(){
		return this.getNum1() - this.getNum2();
	}
	
}
