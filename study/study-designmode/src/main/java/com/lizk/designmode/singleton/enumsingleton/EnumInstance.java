package com.lizk.designmode.singleton.enumsingleton;

/**
 * 使用枚举来实现单例模式
 * 1.访问枚举值的时候,调用枚举的构造方法,枚举的构造方法强制的是private,并且同一个值只被构造一次,天然的实现了只构造一次的要求
 * 2.枚举值INSTANCE在被访问的时候才会调用构造方法,把单例对象放到Enum的构造方法中,天然的实现了懒加载的机制
 */
public enum EnumInstance {
	INSTANCE;
	private User data ;
	EnumInstance() {
		System.out.println("实例化方法");
		data = new User();
		data.setAge(User.random.nextInt(10000));
		data.setName("sdsf");
	}
	
	public User getInstance() {
		return data;
	}
	
}
