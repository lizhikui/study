package com.lizk.designmode.singleton.enumsingleton;

public class Test {
	public static void main(String[] args) throws InterruptedException {

		Thread.sleep(2000);

		EnumInstance.INSTANCE.getInstance();
		User o = EnumInstance.INSTANCE.getInstance();
		System.out.println(o.getName());
		
	}
}
