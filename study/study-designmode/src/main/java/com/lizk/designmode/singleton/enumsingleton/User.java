package com.lizk.designmode.singleton.enumsingleton;

import java.util.Random;

public  class User {
	public final static Random  random = new Random();

	private String name;
	private Integer age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
}
