package com.lizk.designmode.state;

public class BackState implements State{

	@Override
	public void run(Context c) {
		System.out.println("请先停下来");
	}

	@Override
	public void stop(Context c) {
		c.setState(c.getStopState());
		System.out.println("停下来");
	}

	@Override
	public void back(Context c) {
		System.out.println("我正在倒退");
	}

}
