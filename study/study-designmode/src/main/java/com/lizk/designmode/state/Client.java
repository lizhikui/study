package com.lizk.designmode.state;

public class Client {
	public static void main(String[] args) {
		Context c = new Context();
		c.setState(new StopState());
		
		
		c.run();
		c.back();
		c.stop();
		c.run();
		c.run();
		c.back();
		c.stop();
		c.back();
		c.back();
	}
}
