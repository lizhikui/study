package com.lizk.designmode.state;

public class Context {
	private State runState = new RunState();
	private State stopState = new StopState();
	private State backState = new BackState();
	
	private State state;

	public void run(){
		state.run(this);
	}
	public void stop(){
		state.stop(this);
	}
	public void back(){
		state.back(this);
	}
	
	public void setState(State state) {
		this.state = state;
	}
	public State getRunState() {
		return runState;
	}
	public void setRunState(State runState) {
		this.runState = runState;
	}
	public State getStopState() {
		return stopState;
	}
	public void setStopState(State stopState) {
		this.stopState = stopState;
	}
	public State getBackState() {
		return backState;
	}
	public void setBackState(State backState) {
		this.backState = backState;
	}
	public State getState() {
		return state;
	}
	
	
}
