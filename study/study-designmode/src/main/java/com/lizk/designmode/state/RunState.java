package com.lizk.designmode.state;

public class RunState implements State{


	@Override
	public void run(Context c) {
		System.out.println("我已经在跑了");
	}

	@Override
	public void stop(Context c) {
		System.out.println("我要听下来了");
		c.setState(c.getStopState());
	}

	@Override
	public void back(Context c) {
		System.out.println("请先停下来");
	}

}
