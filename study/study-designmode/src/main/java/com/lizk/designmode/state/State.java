package com.lizk.designmode.state;

public interface State {
	public void run(Context c);
	public void stop(Context c);
	public void back(Context c);
}
