package com.lizk.designmode.state;

public class StopState implements State{

	@Override
	public void run(Context c) {
		c.setState(c.getRunState());
		System.out.println("我要开始跑了");
	}

	@Override
	public void stop(Context c) {
		System.out.println("我没跑");
	}

	@Override
	public void back(Context c) {
		c.setState(c.getBackState());
		System.out.println("我要开始倒退了");
	}

}
