package com.lizk.designmode.singleton.enumsingleton;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author lizhikui
 * @date 2020/2/26 15:23
 */
public class EnumInstanceTest {
    @Test
    public void test(){
        Assert.assertTrue(EnumInstance.INSTANCE.getInstance().getAge().equals(EnumInstance.INSTANCE.getInstance().getAge()));
    }
}
