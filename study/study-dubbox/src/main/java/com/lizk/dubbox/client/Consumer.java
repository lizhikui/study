package com.lizk.dubbox.client;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lizk.dubbox.controller.UserController;


public class Consumer {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:applicationContext-consumer.xml" });

		
		UserController userController = (UserController) context.getBean("userController");
		
		context.close();
		userController.client();
		

	}
}
