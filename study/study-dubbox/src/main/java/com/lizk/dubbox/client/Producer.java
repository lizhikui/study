package com.lizk.dubbox.client;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Producer {
	public static void main(String[] args) {
		
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] {"classpath:applicationContext-producer.xml"});
		
		try {
			System.in.read();
			context.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
