package com.lizk.dubbox.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.lizk.dubbox.entity.User;
import com.lizk.dubbox.service.impl.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	/*@Reference(protocol="rest",interfaceClass=com.lizk.dubbox.service.impl.UserService.class,cluster="failsafe")*/
	@Reference(protocol="dubbo",cluster="failsafe")
	private UserService userService;
	
	@ResponseBody
	@RequestMapping("/name")
	public User client(){
		User user = userService.getUser("李志奎",null);
		System.out.println(user);
		return user;
	}
}
