package com.lizk.dubbox.entity;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
//这里个注解使该对象可以被序列化为xml
//@XmlRootElement

//如果需要使用【@XmlElement(name="xml_username")】来设置返回的属性，那么必须加上这句注释
@XmlAccessorType(XmlAccessType.FIELD)
public class User{
	
	//xml定制序列化
	@XmlElement(name="xml_username")
	//json定制序列化
	@JsonProperty("json_username")
	private String name;
	private Integer age;
	private String[] interest;
	private List<User> family;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String[] getInterest() {
		return interest;
	}
	public void setInterest(String[] interest) {
		this.interest = interest;
	}
	public List<User> getFamily() {
		return family;
	}
	public void setFamily(List<User> family) {
		this.family = family;
	}
	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + ", interest=" + Arrays.toString(interest) + ", family=" + family
				+ "]";
	}
	
	
	
	
}
