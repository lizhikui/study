package com.lizk.dubbox.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.lizk.dubbox.entity.User;
import com.lizk.dubbox.service.impl.UserService;

/*@Service(protocol={"dubbo","rest"},interfaceClass=com.lizk.dubbox.service.impl.UserService.class,cluster="failsafe")*/
@Service(protocol={"dubbo","rest"},cluster="failsafe")
@Path("user")
public class UserServiceImpl implements UserService{

	@Path("{name:\\w+}")
	@GET
	@Produces({ContentType.APPLICATION_JSON_UTF_8,ContentType.TEXT_XML_UTF_8})
	@Override
	public User getUser(@PathParam("name") String name,@Context HttpServletRequest request) {
		
		
		//System.out.println(request.getRemotePort());
		User u = new User();
		u.setName(name);
		u.setAge(23);
		u.setInterest(new String []{"读书","写字","打球"});
		
		User u2 = new User();
		u2.setName(name+"的爸爸");
		u2.setAge(45);
		u2.setInterest(new String []{"抽烟","喝酒","烫头"});
		
		User u3 = new User();
		u3.setName(name+"的妈妈");
		u3.setAge(12);
		u3.setInterest(new String []{"逛街","吃饭"});
		
		List<User> list = new ArrayList<User>();
		list.add(u3);
		list.add(u2);
		u.setFamily(list);
		System.out.println("返回数据=========================");
		return u;
	}

}
