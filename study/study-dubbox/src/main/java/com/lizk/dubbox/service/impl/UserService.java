package com.lizk.dubbox.service.impl;

import javax.servlet.http.HttpServletRequest;

import com.lizk.dubbox.entity.User;

public interface UserService {
	public User getUser(String name,HttpServletRequest request);
}
