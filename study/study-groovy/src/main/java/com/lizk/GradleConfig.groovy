package com.lizk

import picocli.CommandLine.Model.ArgSpec

/*
 * ================================================================================
 *  build.gradle配置是采用的groovy DSL语法，
 * 为了更好的理解build.gradle配置的含义,来研究一下DSL是怎么实现的
 *
 *
 * 以下是一个常见的配置项,含义为dependencies是一个方法,方法接受的参数是一个闭包，
 * 大括号内的内容为闭包的内容.闭包中 compile是一个方法,空格后的内容为compile的参数
 * dependencies {
 *      compile 'org.codehaus.groovy:groovy-all:2.3.11'
 * }
 * ================================================================================
 */

def compile(name){
    println "name:$name"
}
def dependencies(closure) {
    closure.call()
}
dependencies {
    compile 'org.codehaus.groovy:groovy-all:2.3.11'
}

/*
 * ================================================================================
 */





/*
 * ================================================================================
 * plugins {
 *      id 'org.springframework.boot' version '2.0.5.RELEASE'
 *      id 'java'
 * }
 * 这也是是一个常用的配置,含义为plugins函数接受一个闭包参数
 * id是一个函数, 接受一个字符串参数,返回值中有一个version方法,接受字符串参数
 * ================================================================================
 */
def plugins(closure){
    closure.call()
}
def id(id){
    println "id:$id"
    return this
}
def version(version){
    println "version:$version"
    return this
}
plugins {
    id 'org.springframework.boot' version '2.0.5.RELEASE'
}
/*
 *================================================================================
 */





/*
 * ================================================================================
 * apply plugin: 'io.spring.dependency-management'
 *
 * 这种配置实际上是apply函数接受了一个map类型的参数
 * ================================================================================
 */
def apply(map){
    for (Map.Entry entry : map.entrySet()) {
        println "K:"+entry.getKey()+";V:"+entry.getValue()
    }
}

apply plugin: 'io.spring.dependency-management'

/*
 * ================================================================================
 */





/**
 * ================================================================================
 * task copyDocs(type: Copy) {
 *      from 'src/main/doc'
 *      into 'build/target/doc'
 * }
 * 这是自定义的一个task，
 * 下面是自己实现的一种方式,通过实验貌似跟gradle的实现上有些区别
 * ================================================================================
 */
class Task {
    def type
    def name
    def doLastClosure
    def doLast(closure){
        doLastClosure = closure
    }
}
def task(String name,Map<String, ?> args,  Closure configureClosure){
    println "task1"
    Task t = new Task()
    t.name = name
    args.each {
        key,value->
            if(t.hasProperty(key))
                t[key] = value
    }
    configureClosure.delegate = t
    configureClosure()
    return t
}

def task(String name, Closure configureClosure){
    println "task2"
    Task t = new Task()
    t.name = name
    configureClosure.delegate = t
    configureClosure()
    return t
}

/**
 * 调用当前对象不存在的方法的时候走这个方法,
 * @param name  方法的名称
 * @param args  方法的参数
 * @return      组成的新的参数列表
 */
def invokeMethod(String name,args){
    def objects= []
    objects.add(name)
    objects.addAll(args)
    objects
}
def task1 = task clean{
    println "clean task"
}

def task2 = task compile(type:"Copy"){
    name = "_compile_"
    doLast {
        println "compile task"
    }
}
println "-----------------"
println task1.name
println task1.type
if(task1.doLastClosure)
    task1.doLastClosure()
println "-----------------"
println task2.name
println task2.type
if(task2.doLastClosure)
    task2.doLastClosure()
println "-----------------"

/*
 * ================================================================================
 */



/**
 * 当获取的属性不存在的时候,默认走这里的逻辑
 * @param name
 * @return
 */
def getProperty(String name){
    "dddddddddddd"
}
println jjjjj
