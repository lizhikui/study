package com.lizk.mongodb;


import org.bson.Document;
import org.bson.conversions.Bson;

import com.alibaba.fastjson.JSON;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexOptions;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	MongoClient mongo = new MongoClient("111.231.201.157", 27017);
    	User user = new User("李志", 25, "男");
    	User user2 = new User();
    	user2.setName("李");
    	MongoCollection<Document> coll = mongo.getDatabase("test").getCollection("test");
    	//coll.dropIndex("$**_text");
    	System.out.println(
    			coll.createIndex(Document.parse("{'$**':'text'}"),new IndexOptions().defaultLanguage("hans"))
    			);
    	coll.insertOne(Document.parse(JSON.toJSONString(user)));
    	for(Document d :coll.listIndexes()) {
    		System.out.println(d.toJson());
    	}
    	
    	
    	for(Document d : coll.find(Document.parse("{$text:{$search:'李'}}"))) {
    		
    		System.out.println(d.toJson());
    	}
    	
    			
    }
}
