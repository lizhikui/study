package com.lizk.net.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Server {
	AsynchronousServerSocketChannel server = null;
	public void start() throws IOException{
		server = AsynchronousServerSocketChannel.open();
		server.bind(new InetSocketAddress("localhost", 10001));
		server.accept(this, new CompletionHandler<AsynchronousSocketChannel, Server>() {

			@Override
			public void completed(AsynchronousSocketChannel result, Server attachment) {
				
				attachment.server.accept(attachment, this);
					
					ByteBuffer buffer = ByteBuffer.allocate(6);
					
					List<Byte> list = new ArrayList<>();
					
					//异步的方式读取处理内容
					result.read(buffer,buffer,new CompletionHandler<Integer, ByteBuffer>(){

						@Override
						public void completed(Integer index, ByteBuffer attachment) {
							
							/*if(index == 0){
								byte [] bb = new byte[list.size()];
								for(int i  = 0 ; i< list.size() ; i ++){
									bb[i] = list.get(i);
								}
								
								System.out.println(new String (bb));
							}*/
							if(index <= -1){
								try {
									System.out.println("关闭连接");
									result.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else{
								attachment.flip();
								byte[] b = null;
								while(attachment.hasRemaining()){
									b = new byte[attachment.remaining()];
									attachment.get(b);
									
									/*for(byte tmpB : b){
										list.add(tmpB);
									}*/
								}
								
								System.out.println(new String (b));
								try {
									result.write(ByteBuffer.wrap("test2".getBytes())).get();
								} catch (InterruptedException | ExecutionException e) {
									e.printStackTrace();
								}
								attachment.clear();
								result.read(buffer, buffer, this);
							}
						}

						@Override
						public void failed(Throwable exc, ByteBuffer attachment) {
							
						}
						
					});
					
					// 同步的方式读取处理内容
					/*try {
						//while (true){
						buffer.clear();
						result.read(buffer).get();
						buffer.flip();
						byte [] dst = new byte[buffer.remaining()];
						buffer.get(dst);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						result.write(ByteBuffer.wrap("test2".getBytes())).get();
						System.out.println(new String(dst));
						
						//}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					System.out.println("------------");
					
					
				
			}

			@Override
			public void failed(Throwable exc, Server attachment) {
				
			}

			
		});
		System.out.println("server 启动");
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				Server server = new Server();
				try {
					server.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		Thread t1 = new Thread(r);
		t1.start();
		Thread.sleep(300000);
	}
}
