package com.lizk.net.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.lizk.net.http.tools.Get;
import com.lizk.net.http.tools.Params;
import com.lizk.net.http.tools.Post;
import com.lizk.net.http.tools.PostBody;
import com.lizk.net.http.tools.Posts;



public class Client {
	public static void main(String[] args) throws Exception {
		
		Params param = new Params();
		param.addParam("name", "admin").
			  addParam("password", "123456");
		String a = PostBody.post("http://localhost:8080/r8/systemLog/list.do", null);
		
		String [] aa = a.split("\r\n");
		for(String tmp : aa) {
			System.out.println(tmp);
			
		}
		
		
		
		/*HttpRequestClient client = new HttpRequestClient();
		System.out.println(new String (client.get("http://www.baidu.com/s?wd=分区助手")));
		
		
		HttpClient client = HttpClients.createDefault();
		HttpGet get  = new HttpGet("http://localhost:8080/r8/manager/login/login.jsp?name=admin&password=123456");

		HttpResponse response1 = client.execute(get);
		
		HttpPost post = new HttpPost("http://localhost:8080/r8/manager/login/login.jsp");
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("name", "admin"));
		params.add(new BasicNameValuePair("password", "123456"));
		HttpEntity entity =  new UrlEncodedFormEntity(params,"utf-8");
		post.setEntity(entity);
		
		System.out.println(EntityUtils.toString(entity));
		
		HttpResponse response2 = client.execute(post);
		
		
		String result = EntityUtils.toString(response1.getEntity(),"utf-8");
		System.out.println(result);*/
		
		
	}
	
	public static String format(String src) throws Exception {
		SAXReader reader = new SAXReader();
	    // System.out.println(reader);
	    // 注释：创建一个串的字符输入流
	    StringReader in = new StringReader(src);
	    Document doc = reader.read(in);
	    // System.out.println(doc.getRootElement());
	    // 注释：创建输出格式
	    OutputFormat formater = OutputFormat.createPrettyPrint();
	    //formater=OutputFormat.createCompactFormat();
	    // 注释：设置xml的输出编码
	    formater.setEncoding("utf-8");
	    // 注释：创建输出(目标)
	    StringWriter out = new StringWriter();
	    // 注释：创建输出流
	    XMLWriter writer = new XMLWriter(out, formater);
	    // 注释：输出格式化的串到目标中，执行后。格式化后的串保存在out中。
	    writer.write(doc);
	 
	    writer.close();
	    System.out.println(out.toString());
	    // 注释：返回我们格式化后的结果
	    return out.toString();
	}
	
}
