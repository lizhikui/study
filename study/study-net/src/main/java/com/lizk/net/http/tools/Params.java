package com.lizk.net.http.tools;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class Params extends ArrayList<NameValuePair>{
	private static final long serialVersionUID = 5408727987830380931L;
	
	public Params addParam(String name,String value) {
		NameValuePair nameValuePair = new BasicNameValuePair(name,value);
		this.add(nameValuePair);
		return this;
	}
}
