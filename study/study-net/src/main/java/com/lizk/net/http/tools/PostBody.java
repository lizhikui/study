package com.lizk.net.http.tools;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class PostBody {
	public static String post(String url, String msg) throws Exception {

		
		
		CloseableHttpClient  client = HttpClients.createDefault();

		HttpEntity entity = null;

		HttpPost post = new HttpPost(url);
		
		if(msg != null && !msg.equals("")) {
			entity = new StringEntity(msg,"utf-8");
			post.setEntity(entity);
		}
		
		try {
			HttpResponse response = client.execute(post);
			HttpEntity resultEntity = response.getEntity();
			return EntityUtils.toString(resultEntity, "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("访问网页失败", e);
		}

	}
}
