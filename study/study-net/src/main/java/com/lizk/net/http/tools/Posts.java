package com.lizk.net.http.tools;

import java.io.File;
import java.io.IOException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

public class Posts {

	public static String post(String url,Params params) throws Exception {
		
		SSLContext sslcontext = SSLContexts.createDefault();
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,new String[] { "TLSv1" },null,SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		
		HttpClient client =  HttpClients.custom().setSSLSocketFactory(sslsf).build();
		
		HttpEntity entity = null;

		HttpPost post  = new HttpPost(url);
		if(params != null) {
			entity = new UrlEncodedFormEntity(params,"utf-8");
			post.setEntity(entity);
		}
		
		try {
			HttpResponse response = client.execute(post);
			HttpEntity resultEntity = response.getEntity();
			return EntityUtils.toString(resultEntity,"utf-8");
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception ("访问网页失败",e);
		}
		
	}

}
