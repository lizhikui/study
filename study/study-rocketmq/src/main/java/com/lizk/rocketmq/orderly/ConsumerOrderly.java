package com.lizk.rocketmq.orderly;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;

import com.lizk.rocketmq.Config;

public class ConsumerOrderly {
	public static void main(String[] args) {
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("CONSUMER_GROUP_ORDER");
		consumer.setNamesrvAddr(Config.NAMESRV_ADDR);
		consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
		try {
			consumer.subscribe("topic_order", "*");
			consumer.setMessageListener(new MessageListenerOrderly() {
				@Override
				public ConsumeOrderlyStatus consumeMessage(List<MessageExt> paramList,
						ConsumeOrderlyContext paramConsumeOrderlyContext) {
							try {
								System.out.println("消费记录：" + new String(paramList.get(0).getBody(),Config.ENCODE_UTF8));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					System.out.println(paramList);
					
					return ConsumeOrderlyStatus.SUCCESS;
				}
			});
			consumer.start();
		} catch (MQClientException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
}
