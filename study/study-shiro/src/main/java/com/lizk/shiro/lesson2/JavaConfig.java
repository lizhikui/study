package com.lizk.shiro.lesson2;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authz.Authorizer;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.realm.jdbc.JdbcRealm.SaltStyle;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;

import com.alibaba.druid.pool.DruidDataSource;

public class JavaConfig {
	public static void main(String[] args) throws IOException {
		
		
		String password=new SimpleHash("MD5","862952","13520731626").toString();
		System.out.println(password);
		
		
		DefaultSecurityManager securityManager = new DefaultSecurityManager(); 
		Authenticator authenticator = new ModularRealmAuthenticator();
		securityManager.setAuthenticator(authenticator);
		
		Authorizer authorizer = new ModularRealmAuthorizer();
		securityManager.setAuthorizer(authorizer);
		
		SessionManager sessionManager = new DefaultSessionManager();
		//securityManager.setSessionManager(sessionManager);
		
		
		
		securityManager.setCacheManager(new MemoryConstrainedCacheManager());
		
		
		JdbcRealm jdbcRealm = new JdbcRealm();
		
		DruidDataSource ds = new DruidDataSource();
		ds.setPassword("654321");
		ds.setUsername("root");
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://10.10.10.104:3306/r8china_app?useUnicode=true&characterEncoding=utf8&generateSimpleParameterMetadata=true");
		
		
		jdbcRealm.setDataSource(ds);
		jdbcRealm.setAuthenticationQuery("select passwd,phone from advisor where cellphone=?");
		
		
		jdbcRealm.setPermissionsLookupEnabled(true);
		jdbcRealm.setPermissionsQuery("SELECT permission_name FROM advisor_role_permission WHERE role_name =?");
		jdbcRealm.setUserRolesQuery("select role_name from advisor where cellphone=? ");
		jdbcRealm.setSaltStyle(SaltStyle.COLUMN);
		jdbcRealm.setCredentialsMatcher(new HashedCredentialsMatcher("MD5"));

		
		securityManager.setRealm(jdbcRealm);
		
		
		
		
		
		SecurityUtils.setSecurityManager(securityManager);
		final Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken("13520731626", "862952");  
		subject.login(token);
		
		System.out.println(subject.isAuthenticated());
		
		subject.checkRole("advisor");
		
		subject.checkPermission("trial-send");
		
		subject.login(token);
		
		System.out.println(subject.getSession().getId());
		System.out.println(SecurityUtils.getSubject()==SecurityUtils.getSubject());
		System.out.println(SecurityUtils.getSubject().getSession()==SecurityUtils.getSubject().getSession());
		System.out.println(Thread.currentThread().getName());
		System.out.println(ThreadContext.get(ThreadContext.SUBJECT_KEY));
		
		new Thread() {
			public void run() {
				System.out.println(Thread.currentThread().getName());
				System.out.println(ThreadContext.get(ThreadContext.SUBJECT_KEY));
				Subject subject2 = SecurityUtils.getSubject();
				System.out.println("----------");
				System.out.println(subject2==subject);
				System.out.println(subject2.getSession().getId());
				System.out.println(subject2.isAuthenticated());
				System.out.println(subject2.isPermitted("sss"));
				System.out.println("----------");
			}
		}.start();
		
		//subject.login(token);
		System.out.println(subject.isAuthenticated());
		
		subject.checkRole("aaa");
		
		subject.checkPermission("sss");
		
		System.in.read();
		
	}
}
