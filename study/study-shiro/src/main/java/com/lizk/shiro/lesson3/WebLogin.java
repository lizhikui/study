package com.lizk.shiro.lesson3;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebLogin {
	
	@RequestMapping("/login")
	public ModelAndView login() {
		
		UsernamePasswordToken token = new UsernamePasswordToken("13520731626", "862952");
		
		SecurityUtils.getSubject().login(token);
		
		return new ModelAndView("../index");
	}
	
	
	
	@RequestMapping("/view")
	@RequiresRoles({"advisor"})
	@RequiresPermissions({"xxx"})
	public ModelAndView view() {
		
		return new ModelAndView("../index");
	}
}
