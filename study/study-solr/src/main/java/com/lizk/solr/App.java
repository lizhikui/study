package com.lizk.solr;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.LBHttpSolrClient;
import org.apache.solr.client.solrj.io.SolrClientCache;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.SolrParams;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SolrServerException, IOException
    {
    	SolrClientCache c = new SolrClientCache();
    	//CloudSolrClient client = c.getCloudSolrClient("192.168.0.11:8983");
    	HttpSolrClient client1 = c.getHttpSolrClient("http://192.168.0.11:8983/solr/core1");
    	HttpSolrClient client2 = c.getHttpSolrClient("http://192.168.0.11:8983/solr/core2");
    	
    	
    	//创建文档
    	/*SolrInputDocument d = new SolrInputDocument();
    	d.setField("id", "005");
    	d.setField("name", "胡雪艳");
    	d.setField("age", "65");
    	System.out.println(client2.add(d));
    	client2.commit();*/
    	
    	
    	//搜索文档
    	SolrQuery sq = new SolrQuery();
    	sq.add("q", "name:曾");
    	sq.addHighlightField("name");
    	sq.setHighlight(true);
    	sq.setHighlightSimplePre("<b>");
    	sq.setHighlightSimplePost("</b>");

    	
    	QueryResponse qr = client2.query(sq);
    	SolrDocumentList sdl = qr.getResults();
    	System.out.println(sdl);
    	for(SolrDocument sd : sdl){
    		System.out.println("id:"+sd.getFieldValue("id"));
    		System.out.println("name:"+sd.getFieldValue("name"));
    		System.out.println("age:"+sd.getFieldValue("age"));
    	}
    	System.out.println(qr.getHighlighting());
    	
    }
}
