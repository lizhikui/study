package com.lizk.thread.akka;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import akka.actor.UntypedAbstractActor;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

public class Greeter extends UntypedAbstractActor {
	public final static String DONE = "DONE";
	public final static String BEGIN = "BEGIN";
	
	@Override
	public void onReceive(Object msg) throws Throwable {
		if (Greeter.BEGIN == msg) {
			for(int i = 0 ; i < 10 ; i ++){
				TimeUnit.SECONDS.sleep(2);
				System.out.println(Thread.currentThread().getName() + this.getSender() +  "我要说话了");
			}
		}else if(Greeter.DONE == msg){
			
			this.getContext().getSystem().stop(getSelf());
		}
	}
	Greeter() {
	}

	@Override
	public void postRestart(Throwable reason) throws Exception {
		super.postRestart(reason);
		System.out.println("重启成功");
	}

	@Override
	public void postStop() throws Exception {
		super.postStop();
		System.out.println("关闭成功");
	}

	@Override
	public void preRestart(Throwable reason, Optional<Object> message) throws Exception {
		super.preRestart(reason, message);
		System.out.println("准备重启");
	}

	@Override
	public void preStart() throws Exception {
		super.preStart();
		System.out.println("准备启动");
//		if(this.getSender() != null){
//			this.getSender().tell(new Object(), this.getSelf());
//		}
	}
	@Override
	public PartialFunction<Object, BoxedUnit> receive() {
		System.out.println("receive");
		return super.receive();
	}
	
}
