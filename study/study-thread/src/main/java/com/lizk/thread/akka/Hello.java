package com.lizk.thread.akka;

import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;

public class Hello {
	public static void main(String[] args) throws InterruptedException {
		ActorSystem system = ActorSystem.create();
		Props greeter = Props.create(Greeter.class);
		Props wacher = Props.create(Wacher.class);
		
		ActorRef wacherArf =  system.actorOf(wacher,"wacher_test");
		
		ActorRef greeterArf =  system.actorOf(greeter,"greeter_test");
		ActorRef greeterArf2 =  system.actorOf(greeter,"greeter_test2");
		
		//调用actor的监视处理逻辑，让一个actor监视另一个actor
		wacherArf.tell(Wacher.WACHER, greeterArf);
		wacherArf.tell(Wacher.WACHER, greeterArf2);
		
		//调用actor的begin处理逻辑
		greeterArf.tell(Greeter.BEGIN, ActorRef.noSender());
		greeterArf2.tell(Greeter.BEGIN, ActorRef.noSender());
		
		TimeUnit.SECONDS.sleep(8);
		
		//停止一个actor
		greeterArf.tell(PoisonPill.getInstance(), ActorRef.noSender());
		greeterArf2.tell(PoisonPill.getInstance(), ActorRef.noSender());
		
		
	}
	
}
