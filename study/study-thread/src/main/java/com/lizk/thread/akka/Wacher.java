package com.lizk.thread.akka;

import akka.actor.ActorRef;
import akka.actor.Terminated;
import akka.actor.UntypedAbstractActor;

public class Wacher extends UntypedAbstractActor{

	public final static String WACHER = "WACHER";
	
	@Override
	public void preStart() throws Exception {
		
	}
	@Override
	public void onReceive(Object msg) throws Throwable {
		if(msg instanceof Terminated){
			ActorRef a  = ((Terminated) msg).getActor();
			System.out.println(a.path() + "关闭");
		}else{
			System.out.println("开启监视");
			//添加监视，在当前的actor上监视他的通知者，可以监视多个actor
			this.getContext().watch(this.getSender());
		}
		
	}

}
