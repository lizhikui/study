package com.lizk.thread.callable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class CallTest  implements Callable<Map<String,Object>>{

	@Override
	public Map<String,Object> call() throws Exception {
		Thread.sleep(3000);
		Map<String,Object> map = new HashMap<>();
		map.put("1", "返回的map");
		return map;
	}

}
