package com.lizk.thread.callable;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
/**
 * callable接口不能创建线程，只有通过new Thread来创建新的线程，
 * callable只是实现了一种future模式异步执行的方式。
 * 
 * @author lizk
 *
 */
public class Client {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		CallTest c = new CallTest();
		FutureTask<Map<String,Object>> f = new FutureTask<>(c);
		
		System.out.println("开始异步的去执行futureTask任务");
		new Thread(f).start();
		
		System.out.println("开始执行其他任务========");
		
		System.out.println("获取异步执行结果：" + f.get().get("1"));
		
		System.out.println("执行最后的任务========end");
	}
}
