package com.lizk.thread.relist;

import java.util.ArrayList;
import java.util.List;

public class Client {
	public static int a = 0;
	static List<String> list = new ArrayList<String>();
	
	public static void main(String[] args) {
		Runnable set = new Runnable(){
			public void run (){
				a = a + 1;
				list.add("11");
			}
		};
		Runnable get = new Runnable(){
			public void run (){
				System.out.print("a="+a+",");
				System.out.println("size="+list.size());
			}
		};
		
		for(int i = 0 ; i < 100 ; i ++){
			new Thread (get).start();
			new Thread (set).start();
		}
	}

}
