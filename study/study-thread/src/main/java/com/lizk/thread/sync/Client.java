package com.lizk.thread.sync;

public class Client {
	public static void main(String[] args) throws InterruptedException {
		final TestSynchronized t = new TestSynchronized();
		final TestSynchronized t2 = new TestSynchronized();
		
		
		new Thread(){
			@Override
			public void run() {
				t.print();
			}
		}.start();
		
		Thread.sleep(1000);
		
		new Thread(){
			@Override
			public void run() {
				t.print2();
			}
		}.start();
	}
}
