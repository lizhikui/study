package com.lizk.thread.sync;


/**
 * synchronized修饰在方法上，那么类的同一个实例中的所有被sync修饰方法都是互斥的，
 * @author lizk
 *
 */
public class TestSynchronized {
	private Integer i = 0;
	public synchronized void print(){
			i = i+1;
			try {
				int l = 1/0;
				Thread.sleep(5000);
			} catch (Exception e) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			System.out.println("print1:"+i);
	}
	
	public synchronized void print2(){
		i = i+1;
		System.out.println("print2:"+i);
	}
}
