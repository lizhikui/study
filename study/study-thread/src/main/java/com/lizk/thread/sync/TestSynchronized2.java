package com.lizk.thread.sync;


/**
 * synchronized修饰在【静态】方法上,等同于在类级别上加锁，所有在被Sync修饰的静态方法都是互斥的
 * @author lizk
 *
 */
public class TestSynchronized2 {
	private static Integer i = 0;
	public static synchronized void print(){
			i = i+1;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("print1:"+i);
	}
	
	public static synchronized void print2(){
		i = i+1;
		System.out.println("print2:"+i);
	}
}
