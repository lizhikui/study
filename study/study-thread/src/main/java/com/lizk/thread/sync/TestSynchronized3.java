package com.lizk.thread.sync;


/**
 * synchronized修饰在静态方法和普通方法上分别等于在类上加锁，和在this上加锁，所以这两个方法不是互斥的
 * @author lizk
 *
 */
public class TestSynchronized3 {
	private static Integer i = 0;
	public static synchronized void print(){
			i = i+1;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("print1:"+i);
	}
	
	public synchronized void print2(){
		i = i+1;
		System.out.println("print2:"+i);
	}
}
