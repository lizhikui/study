package com.lizk.thread.sync;
/**
 * synchronized 修饰在类中的对象上的时候，锁定在同一个对象上的方法互斥
 * @author Administrator
 *
 */
public class TestSynchronized4 {
	private Object o = new Object();//直接new一个对象来做锁对象
	private Integer i = 0;//由于Integer0-128对象是同一个，所以不要用Integer做锁对象，同样的也不要用string类型做锁对象，
	public void print(){
		synchronized(i){
			i = i+1;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("print1:"+i);
		}
	}
	
	public void print2(){
		synchronized(i){
		i = i+1;
		System.out.println("print2:"+i);
		}
	}
}
