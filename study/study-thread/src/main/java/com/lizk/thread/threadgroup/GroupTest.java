package com.lizk.thread.threadgroup;

import java.util.concurrent.locks.ReentrantLock;

public class GroupTest {
	public static void main(String[] args) throws InterruptedException {
		
		Runnable r = new Runnable(){
			@Override
			public void run() {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("11111");
			}
		};
		
		ThreadGroup tg = new ThreadGroup("groupFarther");
		ThreadGroup tg2 = new ThreadGroup(tg, "groupchild");
		
		Thread t1 = new Thread(tg2,r,"t1");
		t1.setPriority(1);
		
		Thread t2 = new Thread(tg2,r,"t2");
		t2.setPriority(9);
		
		//Thread.sleep(100);
		System.out.println(tg.activeGroupCount());
		System.out.println(tg.activeCount());
		System.out.println(tg2.activeCount());
		tg.list();

		
	}
}
