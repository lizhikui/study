package com.lizk.thread.timer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * 可以并发的执行定时任务
 * @author lizk
 *
 */
public class ExecuteScheduleTest implements Runnable{
	String name ;
	ExecuteScheduleTest(String name){
		this.name = name;
	}
	
	public static void main(String[] args) {
		
		//设置线程数量以后，会把多余的线程还存在队列中等待，但是最少会启动一个线程来执行任务即使设置成了0
		ScheduledExecutorService  e = Executors.newScheduledThreadPool(2);
		//固定间隔时间执行那个，如果执行时间大于间隔时间，那么第一次执行结束马上开始第二次执行
		//e.scheduleAtFixedRate(new ExecuteScheduleTest("1"), 1, 2, TimeUnit.SECONDS);
		
		//指定开始执行时间，只执行一次
		//e.schedule(new ExecuteScheduleTest("1"), 2, TimeUnit.SECONDS);
		
		//固定间隔时间，以结束之间为准间隔固定时间
		e.scheduleWithFixedDelay(new ExecuteScheduleTest("1"), 0, 2, TimeUnit.SECONDS);
		e.scheduleWithFixedDelay(new ExecuteScheduleTest("2"), 0, 2, TimeUnit.SECONDS);
		e.scheduleWithFixedDelay(new ExecuteScheduleTest("3"), 0, 2, TimeUnit.SECONDS);
	}

	@Override
	public void run() {
		System.out.println(name + "begin"+System.currentTimeMillis()+Thread.currentThread().getName());
		/*if(name.equals("2")) {
			try{
				int a = 1/0;
			}catch(Exception ee){
				ee.printStackTrace();
				throw ee;
			}
		}*/
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(name + " end "+System.currentTimeMillis()+Thread.currentThread().getName());
		
	}
}
