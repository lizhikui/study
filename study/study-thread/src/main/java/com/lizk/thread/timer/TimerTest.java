package com.lizk.thread.timer;

import java.util.Timer;
import java.util.TimerTask;


/**
 * 只适合做单个线程的定时任务执行，同时启动多个任务会报异常“ java.lang.IllegalStateException: Task already scheduled or cancelled”
 * @author lizk
 *
 */
public class TimerTest {
	static TimerTask tt = new TimerTask() {
		@Override
		public void run() {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("开始执行定时任务");
		}
	};
	
	public static void main(String[] args) {
		Timer t = new Timer();
		//只执行一次
		t.schedule(tt, 2000, 5000);
		t.schedule( tt , 1000L);
	}
	
}
