package com.lizk.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.RetryPolicy;

public class Curator {
	private static RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 2);
	private static CuratorFramework cf  = CuratorFrameworkFactory.builder()
			.connectString("47.92.95.2:2181,47.92.95.2:2182,47.92.95.2:2183")
			.sessionTimeoutMs(1000)
			.connectionTimeoutMs(1000)
			//设置充实策略
			.retryPolicy(retryPolicy)
			//设置命名空间，所有的操作都会在这个根节点下进行
			.namespace("test")
			.build();
	
	static{
		cf.start();
	}
	
	public static CuratorFramework get() {
		return cf;
	}
	public static void main(String[] args) {
		Curator.get();
	}
}
