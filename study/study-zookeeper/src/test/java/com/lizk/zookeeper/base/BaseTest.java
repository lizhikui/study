package com.lizk.zookeeper.base;


import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lizk.zookeeper.Curator;

public class BaseTest {
	
	CuratorFramework curator  = null;
	
	@Before
	public void init(){
		curator = Curator.get();
	}
	
	@After
	public void close(){
		curator.close();
	}
	
	/**
	 * 创建节点
	 * @throws Exception
	 */
	@Test
	public void createNode() throws Exception {
		System.out.println(curator.create().forPath("/test","测试一下".getBytes()));
	}
	
	
	/**
	 * 检查节点是否存在
	 * @throws Exception
	 */
	@Test
	public void existNode() throws Exception {
		
		if(curator.checkExists().forPath("/test")!=null){
			Stat stat = curator.checkExists().forPath("/test");
			System.out.println("存在节点test" );
			System.out.println(stat.getVersion());
			System.out.println(stat.getAversion());
			System.out.println(stat.getCtime());
			System.out.println(stat.getCversion());
			System.out.println(stat.getDataLength());
		}
	}
	
	
	/**
	 * 获取节点内容
	 * @throws Exception
	 */
	@Test
	public void getNode() throws Exception {
		
			System.out.println("test节点:" + new  String(curator.getData().forPath("/test")));
	}
	
	/**
	 * 删除节点
	 * @throws Exception
	 */
	@Test
	public void delNode() throws Exception {
		curator.delete().deletingChildrenIfNeeded().forPath("/test");
	}
	
	
	
	
	
}
