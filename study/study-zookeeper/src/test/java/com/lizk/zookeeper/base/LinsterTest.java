package com.lizk.zookeeper.base;

import java.util.List;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lizk.zookeeper.Curator;

public class LinsterTest {
	CuratorFramework curator  = null;
	@Before
	public void init(){
		curator = Curator.get();
	}
	
	@After
	public void close(){
		curator.close();
	}
	
	@Test
	public void createListenr() throws Exception{
		curator.checkExists().usingWatcher(new CuratorWatcher(){

			@Override
			public void process(WatchedEvent paramWatchedEvent) throws Exception {
				System.out.println(paramWatchedEvent.getPath());
				System.out.println(paramWatchedEvent.getState());
				System.out.println(paramWatchedEvent.getType());
				System.out.println(paramWatchedEvent.getWrapper());
			}
			
		}).forPath("/test");
		Thread.currentThread().join();
	}
	
	
	/**
	 * 使用PathChildrenCache 来监听子节点的变化，新增删除修改
	 */
	@Test
	public void testPathChildrenCache() throws Exception{
		/*
		 * 
		 * 
		 * 执行一下命令测试
		 * create /test/test/1 11
		 * set /test/test/1 22
		 * delete /test/test/1
		 * 
		 */
		
		PathChildrenCache pathChildrenCache = new PathChildrenCache(curator, "/test", true);
		pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {
			@Override
			public void childEvent(CuratorFramework arg0, PathChildrenCacheEvent event) throws Exception {
				System.out.println("======== catch children change =======");
                System.out.println("update event type:" + event.getType() +
                        ",path:" + event.getData().getPath() + ",data:" + new String(event.getData().getData(), "UTF-8"));

                List<ChildData> childDataList = pathChildrenCache.getCurrentData();
                if (childDataList != null && childDataList.size() > 0) {
                    System.out.println("path all children list:");
                    for (ChildData childData : childDataList) {
                        System.out.println("path:" + childData.getPath() + "," + new String(childData.getData(), "UTF-8"));
                    }
                }
			}
		});
		pathChildrenCache.start(); 
		
		Thread.currentThread().join();
	}
	
	
	/**
	 * 使用NodeCache 来监听本身节点的变化，新增删除修改
	 */
	@Test
	public void testNodeCache() throws Exception{
		/*
		 * 
		 * 
		 * 执行一下命令测试
		 * create /test/test/ 11
		 * set /test/test/ 22
		 * delete /test/test/
		 * 
		 */
		
		NodeCache nodeCache = new NodeCache(curator, "/test");
		nodeCache.getListenable().addListener(new NodeCacheListener() {

			@Override
			public void nodeChanged() throws Exception {
				System.out.println("======== catch children change =======");
				
				if(nodeCache != null){
					System.out.println(new String (nodeCache.getCurrentData().getData()));
				}else{
					System.out.println("-------");
				}
			}
		});
		nodeCache.start(); 
		
		Thread.currentThread().join();
	}
	
}
