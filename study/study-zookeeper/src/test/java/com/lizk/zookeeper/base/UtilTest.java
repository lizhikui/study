package com.lizk.zookeeper.base;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.atomic.AtomicValue;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicInteger;
import org.apache.curator.framework.recipes.barriers.DistributedBarrier;
import org.apache.curator.framework.recipes.barriers.DistributedDoubleBarrier;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListener;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.state.ConnectionState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lizk.zookeeper.Curator;

public class UtilTest {
	CuratorFramework curator  = null;
	
	@Before
	public void init(){
		curator = Curator.get();
	}
	
	@After
	public void close(){
		curator.close();
	}
	
	
	
	
	
	/**
	 * -------------为选举测试做的准备工作-----------------------
	 */
	
	//为了让创建的线程能够同时开始运行，竞争更激烈
	CyclicBarrier cb = new CyclicBarrier(3);

	
	//线程类，利用多线程模拟多客户端竞争leader
	class Leader implements Runnable{
		@Override
		public void run() {
			try {
				//线程都会在这里等待，直到三个线程都创建了，开始同时运行
				cb.await();
				System.out.println(Thread.currentThread().getName());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
			
			//开始运行以后执行选举处理方法
			testLeaderSelector();
		}
		
		public void testLeaderSelector(){
			
			//创建一个Curator的选举工具类LeaderSelector，传入客户端，操作的节点，和一个监听处理
			//监听可以是LeaderSelectorListener，也可以是LeaderSelectorListenerAdapter，主要实现“takeLeadership”方法，得到leader以后会执行这个方法
			LeaderSelector ls = new LeaderSelector(Curator.get(), "/leader_select", new LeaderSelectorListener() {
				@Override
				public void stateChanged(CuratorFramework cf, ConnectionState state) {
					System.out.println(state);
					System.out.println(Thread.currentThread().getName() + "+++++++++++");
				}
				
				@Override
				public void takeLeadership(CuratorFramework cf) throws Exception {
					System.out.println(Thread.currentThread().getName() + "------------");
				}
			});
			
			//释放leader权限以后，自动再次发起leader的竞争
			//ls.autoRequeue();
			//ls.requeue();
			ls.start();
		}
		
	}
	//---------------------------------------------------------------------------------------------
	
	
	
	/**
	 * 选举测试
	 * @throws InterruptedException 
	 */
	@Test
	public void testLeaderSelectorClient() throws InterruptedException{
		new Thread(new Leader(),"t1").start();
		new Thread(new Leader(),"t2").start();
		new Thread(new Leader(),"t3").start();
		Thread.currentThread().join();
	}
	
	
	
	/**
	 * 测试分布式计数器做准备的工作
	 * @author lizk
	 *
	 */
	class Increment implements Runnable{
		@Override
		public void run() {
			DistributedAtomicInteger a = new DistributedAtomicInteger(Curator.get(), "/integer", curator.getZookeeperClient().getRetryPolicy());
			try {
				cb.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
			for(int i = 0 ; i < 10 ; i ++){
				try {
					AtomicValue<Integer> value = a.increment();
					//因为自增可能会失败，所以必须对自增的返回值做判断
					//value.succeeded()是否成功
					System.out.println(value.succeeded() +"-" +Thread.currentThread().getName() + "-" + value.preValue());
					TimeUnit.MILLISECONDS.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
	/**
	 * 分布式计数器
	 * @throws Exception
	 */
	@Test
	public void incrementNode() throws Exception {
		new Thread(new Increment(),"t1").start();
		new Thread(new Increment(),"t2").start();
		new Thread(new Increment(),"t3").start();
		Thread.currentThread().join();
	}
	
	
	/**
	 * 测试栅栏功能,功能类似CyclicBarrier，但是可以在不同的jvm之间实现
	 * 
	 * 主要是以下几个方法
	 * setBarrier();//设置栅栏
	 *  waitOnBarrier()//在设置的栅栏上等待
	 *  removeBarrier();//移除栅栏
	 * @throws Exception 
	 * 
	 * 
	 */
	@Test
	public void barrierTest() throws Exception{
		DistributedBarrier barrier = new DistributedBarrier(curator, "/barrier");
		barrier.setBarrier();
		new Thread("t1"){

			@Override
			public void run() {
				DistributedBarrier barrier2 = new DistributedBarrier(curator, "/barrier");
				try {
					barrier2.waitOnBarrier();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + "放开栅栏了，快跑");
			}
			
		}.start();
		new Thread("t2"){

			@Override
			public void run() {
				DistributedBarrier barrier2 = new DistributedBarrier(curator, "/barrier");
				try {
					barrier2.waitOnBarrier();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + "放开栅栏了，快跑");
			}
			
		}.start();
		
		TimeUnit.SECONDS.sleep(3);
		
		barrier.removeBarrier();
		Thread.currentThread().join();
	}
	
	/**
	 * 双栅栏测试
	 * 调用enter的时候等待指定数量的对象进入才开始下面的处理
	 * 调用leave的时候同样等待指定数量的对象在进行下面的处理
	 */
	@Test
	public void doubleBarrierTest() throws Exception{
		

		new Thread("t1"){
			DistributedDoubleBarrier barrier = new DistributedDoubleBarrier(curator, "/doublebarrier",2);
			@Override
			public void run() {
				
				try {
					System.out.println(Thread.currentThread().getName() +"我进来了，等待另一个");
					barrier.enter();
					
					System.out.println("我们一起来处理");
					TimeUnit.SECONDS.sleep(3);
					System.out.println("我才处理完");
					
					barrier.leave();
					System.out.println(Thread.currentThread().getName() + "我出来了");
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}.start();
		new Thread("t2"){

			@Override
			public void run() {
				DistributedDoubleBarrier barrier = new DistributedDoubleBarrier(curator, "/doublebarrier",2);
				try {
					TimeUnit.SECONDS.sleep(3);
					System.out.println(Thread.currentThread().getName() +"我也进来了");
					barrier.enter();
					System.out.println("我们一起来处理");
					TimeUnit.SECONDS.sleep(1);
					System.out.println("我已经处理完");
					
					barrier.leave();
					System.out.println(Thread.currentThread().getName() + "我出来了");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}.start();
		
		TimeUnit.SECONDS.sleep(3);
		
		Thread.currentThread().join();
	}
	
	/**
	 * 分布式锁
	 * --InterProcessMutex 重入锁
	 * acquire 获取锁
	 * release 释放锁
	 * makeRevocable 撤销机制，设置了一个撤销的监听器
	 * attemptRevoke 申请撤销
	 * 
	 */
	static int i = 0;
	@Test
	public void InterProcessMutexTest() throws InterruptedException{
		new Thread("t1"){
			@Override
			public void run() {
				InterProcessMutex lock = new InterProcessMutex(Curator.get(),"/InterProcessMutex");
				try {
					for(int a = 0 ; a < 100; a ++){
					lock.acquire();
					i = i+1;
					Thread.sleep(100);
					System.out.println(i);
					lock.release();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		}.start();
		new Thread("t1"){
			@Override
			public void run() {
				InterProcessMutex lock = new InterProcessMutex(Curator.get(),"/InterProcessMutex");
				try {
					for(int a = 0 ; a < 100 ; a ++){
						lock.acquire();
						i = i+1;
						System.out.println(i);
						lock.release();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}.start();
		Thread.currentThread().join();
	}
	
	
	
	
	
	
	
	
}
