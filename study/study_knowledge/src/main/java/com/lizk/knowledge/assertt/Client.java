package com.lizk.knowledge.assertt;

/**
 * @author lizhikui
 * @date 2020/2/21 11:29
 */
public class Client {
    public static void main(String[] args) {
        /*
          断言：不能作为正常运行程序的逻辑判断使用,只能作为程序调试使用
          1.断言设计的目的就是为了方便代码的调试
          2.Java中JVM默认是关闭断言功能的,需要通过-enableassertions或者-ea打开断言
         */
        int i = 0 ;
        assert i == 0;
        assert i == 1;
        System.out.println("==============");
    }
}
