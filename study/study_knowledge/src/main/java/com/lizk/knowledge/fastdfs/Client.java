package com.lizk.knowledge.fastdfs;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.csource.common.MyException;

public class Client {
	public static void main(String[] args) throws FileNotFoundException, IOException, MyException {
		UploadFile u = new UploadFile();
		u.upload();
	}
}
