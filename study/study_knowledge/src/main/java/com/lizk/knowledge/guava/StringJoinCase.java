package com.lizk.knowledge.guava;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import java.util.List;

public class StringJoinCase {
    public static void main(String[] args) {
        List<String> strList = Lists.newArrayList("abc","bcd","   ef g    ",null,"   "," -- ");
        List<String> strSecondList = Lists.newArrayList("a","b","c");


        //存在null会报错
        //System.out.println(JSON.toJSONString(String.join(":", strList)));

        Joiner joiner = Joiner.on(":").useForNull("__");
        System.out.println(JSON.toJSONString(joiner.join(strList)));
        System.out.println(JSON.toJSONString(joiner.join(strSecondList)));

    }
}
