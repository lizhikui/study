package com.lizk.knowledge.guava;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Splitter;

import java.util.List;

public class StringSplitterCase {
    public static void main(String[] args) {
        String a =",,ca,   ,   b  c  ,";
        List<String> strList = Splitter.on(',').omitEmptyStrings().trimResults().splitToList(a);

        System.out.println(JSON.toJSONString(strList));
        System.out.println(JSON.toJSONString(a.split(",")));
    }
}
