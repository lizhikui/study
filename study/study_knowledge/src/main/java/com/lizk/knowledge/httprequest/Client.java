package com.lizk.knowledge.httprequest;


import okhttp3.Call;
import okhttp3.OkHttp;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * @author lizhikui
 * @date 2020/6/2 14:13
 */
public class Client {
    public static void main(String[] args) throws IOException {


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().get().url("http://www.baidu.com").build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()){
            System.out.println(response.body().string());
        }


    }
}
