package com.lizk.knowledge.java8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.lizk.knowledge.java8.function.VoidFunction;
/**
 * 测试jdk8的新特性lamb表达式
 * @author lizk
 *
 */
public class Client {
	public static void main(String[] args) {
		
		//这个接口必须是@FunctionalInterface，或者只有一个接口方法
		//这种写法有点类似匿名实现接口Function，“(c) -> c * 2”为接口实现的具体处理部分，其中(c)为传递的参数
		Function <Integer,Integer> converter = (c) -> c * 2 ;
		System.out.println(converter.apply(2));
		
		
		//具体的处理方法可以｛｝括起来
		Function <Integer,Integer> f1 = (s) -> {
			int i  =  s + 2;
			return i;
		}; 
		System.out.println(f1.apply(10));
		
		//先调用f1，然后结果当做参数再调用converter
		System.out.println(f1.andThen(converter).apply(2));
		
		//自定义的接口类型的函数
		VoidFunction f3 = () -> {System.out.println("f3打印");};
		f3.apply();
		
		Client c = new Client();
		VoidFunction f4 = c::toString;
		f4.apply();
		
		
		//list的迭代
		List<String> list = new ArrayList<>();
		list.add("111");list.add("222");list.add("333");list.add("444");
		list.forEach(System.out::println);
		
		
		//map的迭代
		Map<String,Object> map = new HashMap<>();
		map.put("111", "aaaa");
		map.put("222", "bbbb");
		map.put("333", "cccc");
		map.put("444", "dddd");
		map.forEach((a,b) -> {System.out.print(a);System.out.print("->");System.out.println(b);});
		
		
	}

	@Override
	public String toString() {
		System.out.println("ssssssssss");
		return "ssssssssss";
	}
	
}
