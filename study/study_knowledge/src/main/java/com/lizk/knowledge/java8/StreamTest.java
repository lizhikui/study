package com.lizk.knowledge.java8;

import java.util.Arrays;

public class StreamTest {
	public void print (int i){
		System.out.println(i);
	}
	public static void main(String[] args) {
		int [] arr = new int[]{1,2,3,4,5,6,7,8,9};
		StreamTest st = new StreamTest();
		Arrays.stream(arr).forEach(st :: print);
	}
}
