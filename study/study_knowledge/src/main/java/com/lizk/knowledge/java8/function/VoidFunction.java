package com.lizk.knowledge.java8.function;

@FunctionalInterface
public interface VoidFunction {
	public void apply();
}
