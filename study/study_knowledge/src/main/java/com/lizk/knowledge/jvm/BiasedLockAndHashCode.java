package com.lizk.knowledge.jvm;

/**
 * @author lizhikui
 * @date 2020/9/12 9:53
 */
public class BiasedLockAndHashCode {
    /**
     * 对象的默认哈希值会存放在对象头信息中,这样对象就不能进入偏向锁的状态,
     * 这个例子尝试测试偏向锁效率高于普通的锁.
     * @param args
     */
    public static void main(String[] args) {
        final Object o = new Object();
        System.out.println(o.hashCode());
        for (int j = 0; j < 10; j++) {
            long begin = System.nanoTime();
            for (int i = 0; i < 10000; i++) {
                synchronized (o) {

                }
            }
            long end = System.nanoTime();
            System.out.println(end - begin);
        }
    }

}
