package com.lizk.knowledge.jvm;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author lizhikui
 * @date 2020/9/24 10:03
 */
public class HeapAnalysisZThreadPool {


    public static void main(String[] args) {
        testHeap();
    }

    /**
     * 模拟线程池不停的被添加任务,造成内存占用高.
     */
    static void testHeap(){
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        while (true){
            executorService.submit(() -> {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}
