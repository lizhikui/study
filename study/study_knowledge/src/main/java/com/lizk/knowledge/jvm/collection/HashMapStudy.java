package com.lizk.knowledge.jvm.collection;

import com.lizk.tools.base.SpeedStat;

/**
 * @author lizhikui
 * @date 2020/9/24 9:34
 */
public class HashMapStudy {
    public static void main(String[] args) {
        testMod();
    }

    /**
     * 测试按位与取模的正确性.
     */
    public static void testMod(){
        for (int i = 0; i < 100; i++) {
            System.out.print(i%4);
            System.out.print(':');
            System.out.print(i&3);

            System.out.print('-');

            System.out.print(i%5);
            System.out.print(':');
            System.out.print(i&4);
            System.out.println();
        }
    }

    /**
     * 测试按位与取模的速度.
     */
    public static void testModSpeed(){

        int result ;
        SpeedStat.begin();
        for (int i = 0; i < 10000; i++) {
            result = i%4;
        }
        SpeedStat.end();

        SpeedStat.begin();
        for (int i = 0; i < 10000; i++) {
            result = i&3;
        }
        SpeedStat.end();
    }
}
