package com.lizk.knowledge.jvm.collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author lizhikui
 * @date 2020/9/14 14:27
 */
public class ListRemoveMethod {
    public static void main(String[] args) {
        testRemove();
    }

    public static void testRemove(){
        List<String> list = new ArrayList<String>() {{
            add("2");
            add("3");
            add("3");
            add("3");
            add("4");
        }};

        /*
         * 普通的for循环可以执行,但是逻辑是不对的,remove的元素后面的元素会前移，但是索引还是增加的,导致少处理元素
         */
       /* for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("3")) {
                list.remove(i);
            }
        }*/

        /*
         * 用增强for循环会报错
         */
        /*int i = 0;
        for (String tmp : list){
            if (tmp.equals("3")) {
                list.remove(i);
            }
            i++;
        }*/


        /*
         * 使用迭代器的时候删除,如果使用list.remove也是会报错的,但是可以使用iterator.remove()
         * 因为会修改modCount和expectedModCount的值,当在次调用迭代器的next的方法时，判断两个值是相等的,不会报错.
         */
        Iterator<String> iterator = list.iterator();
        int i = 0 ;
        while (iterator.hasNext()){
            String next = iterator.next();
            if (next.equals("3")) {
//                list.remove(i);//报错
                iterator.remove();//不报错
            }
            i++;
        }
        System.out.println(list);
    }


}
