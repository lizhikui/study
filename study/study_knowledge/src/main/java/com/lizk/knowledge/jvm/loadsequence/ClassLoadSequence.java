package com.lizk.knowledge.jvm.loadsequence;

/**
 * @author lizhikui
 * @date 2020/9/11 12:55
 */
public class ClassLoadSequence {
    public static void main(String[] args) throws Exception {
        ClassLoadSequence classLoadSequence = new ClassLoadSequence();
        classLoadSequence.test();
    }

    /**
     * 验证类的部分加载顺序和初始化时机
     */
    public  void test(){
//        System.out.println(WaitLoadClass.class);//不会初始化
//        System.out.println(WaitLoadClass.class.getField("parentFinal"));//不会初始化
//        System.out.println(WaitLoadClass.parentStaticFinal);//不会初始化
//        System.out.println(Class.forName("com.lizk.knowledge.jvm.loadsequence.WaitLoadClass"));//会初始化
//        System.out.println(WaitLoadClass.class.getField("parentStaticFinal").get(null));//会初始化


//        System.out.println(ChildWaitLoadClass.childStaticFinal);//父类和子类都不会初始化
//        System.out.println(WaitLoadClass.parentStatic);//父类初始化，子类不初始化
//        System.out.println(ChildWaitLoadClass.childStatic);//父类不初始化,子类初始化
//        System.out.println(new ChildWaitLoadClass());//父类不初始化,子类初始化


//        System.out.println(Child2WaitLoadClass.child2StaticFinal); //父类和子类都不会初始化
    }
}
