package com.lizk.knowledge.jvm.loadsequence;

/**
 * @author lizhikui
 * @date 2020/9/11 12:56
 */
public class WaitLoadClass {
    public final String parentFinal = "parentFinal";
    public final static String parentStaticFinal = "parentStaticFinal";
    public static String parentStatic = "parentStatic";
    public String parent = "parent";

    static {
        System.out.println("parentMethod,init");
    }

    static class ChildWaitLoadClass{
        public final String childFinal = "childFinal";
        public final static String childStaticFinal = "childStaticFinal";
        public static String childStatic = "childStatic";
        public String child = "child";
        static {
            System.out.println("ChildMethod,init");
        }

    }

    public class Child2WaitLoadClass{
        public final String child2Final = "child2Final";
        public final static String child2StaticFinal = "child2StaticFinal";
        public String child2 = "child2";
    }

}
