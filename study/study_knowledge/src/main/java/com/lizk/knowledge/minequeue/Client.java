package com.lizk.knowledge.minequeue;

import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class Client {
	public static void main(String[] args) {
		final MineQueue q = new MineQueue();
		
		final AtomicInteger index = new AtomicInteger(0);
		
		int threadCount = 500;
		final Random r = new Random();
		
		//final CountDownLatch count = new CountDownLatch(threadCount);
		
		Runnable put = new Runnable(){
			@Override
			public void run() {
				int tmp = index.incrementAndGet();
				try {
					Thread.sleep(r.nextInt(500));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				q.put(tmp);
				
				
			}
		};
		Runnable take = new Runnable(){
			@Override
			public void run() {
				try {
					Thread.sleep(r.nextInt(500));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				q.take();
			}
		};
	
		for(int i = 0 ; i < threadCount ; i ++){
			new Thread(put).start();
			new Thread(take).start();
			if(i == threadCount -1){
				System.out.println("开始并发执行");
			}
		}
		
		/*new Thread(take).start();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		new Thread(put).start();*/
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("消费结果:" + q.list);
		System.out.println("写入顺序:" + q.putList);
		System.out.println("读取顺序:" + q.takeList);
	
			System.out.println();
			System.out.println();
		
	}
}
