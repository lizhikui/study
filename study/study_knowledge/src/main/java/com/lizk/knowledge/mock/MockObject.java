package com.lizk.knowledge.mock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lizhikui
 * @date 2020/2/25 17:01
 */
public class MockObject {

    private static final Logger logger = LoggerFactory.getLogger(MockObject.class);
    private String name;
    private String phone;
    private Integer age;


    public String say(String name){
        logger.info("hello " + name);
        return name;
    }

    public String getName() {
        logger.info("getName 被调用了");
        return name;
    }

    public void setName(String name) {
        logger.info("setName 被调用了");
        this.name = name;
    }

    public String getPhone() {
        logger.info("getPhone 被调用了");
        return phone;
    }

    public void setPhone(String phone) {
        logger.info("setPhone 被调用了");
        this.phone = phone;
    }

    public Integer getAge() {
        logger.info("getAge 被调用了");
        return age;
    }

    public void setAge(Integer age) {
        logger.info("setAge 被调用了");
        this.age = age;
    }
}
