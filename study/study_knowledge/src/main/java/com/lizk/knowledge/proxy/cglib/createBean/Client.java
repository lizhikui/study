package com.lizk.knowledge.proxy.cglib.createBean;

import java.lang.reflect.InvocationTargetException;

import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.beans.BeanMap;

/**
 * cglib创建实体类
 * @author lizk
 *
 */
public class Client {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException, NoSuchMethodException, InvocationTargetException {
		
		//创建bean的生成器
		BeanGenerator bg = new BeanGenerator();
		//设置要创建类的属性
		bg.addProperty("name", String.class);
		bg.addProperty("age", Integer.class);
		
		//创建实体类
		Object o = bg.create();
		//创建一个beanMap，用来给属性赋值
		BeanMap beanMap = BeanMap.create(o);
		//给属性复制
		beanMap.put("name", "lizhikui");
		beanMap.put("age", 12);
		
		//利用反射调用生成类的方法
		System.out.println(o.getClass().getMethod("getName").invoke(o));
	}
}
