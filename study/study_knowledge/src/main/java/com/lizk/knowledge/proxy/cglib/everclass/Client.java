package com.lizk.knowledge.proxy.cglib.everclass;

import java.lang.reflect.InvocationTargetException;

import org.objectweb.asm.ClassReader;
import org.springframework.asm.ClassVisitor;
import org.springframework.cglib.core.AbstractClassGenerator;

import com.lizk.knowledge.proxy.cglib.proxy.Car;

import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import net.sf.cglib.transform.ClassReaderGenerator;
import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.core.ClassGenerator;
import net.sf.cglib.core.DefaultGeneratorStrategy;
/**
 * cglib实现的反射
 * @author lizk
 *
 */
public class Client {
	public static void main(String[] args) throws InvocationTargetException {
		
		FastClass fc = FastClass.create(Car.class);
		Object o = fc.newInstance();
		FastMethod fm = fc.getMethod("move",null);
		fm.invoke(o, null);
		
		DefaultGeneratorStrategy creater = DefaultGeneratorStrategy.INSTANCE;
		
		
	}
}
