package com.lizk.knowledge.proxy.cglib.proxy;

/**
 * <B>描述:</B>
 * 被代理的类
 * @author lizk
 * @date 2017年3月9日 14:04:06 
 * @version 1.0
 */
public class Car{
	
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Car在移动");
	}

	
}
