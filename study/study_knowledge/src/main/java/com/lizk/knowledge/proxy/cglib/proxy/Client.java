package com.lizk.knowledge.proxy.cglib.proxy;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;


/**
 * 用cglib实现动态代理功能
 * @author lizk
 *
 */
public class Client {
	public static void main(String[] args) {
		Enhancer en = new Enhancer ();
		//设置动态代理类的父类
		en.setSuperclass(Car.class);
		//设置创建的代理类要执行的方法
		en.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object arg0, Method arg1, Object[] arg2, MethodProxy arg3) throws Throwable {
				System.out.println("b");
				//Object o = arg1.invoke(arg0, arg2);
				
				Object o = arg3.invokeSuper(arg0, args); 
				
				System.out.println("e");
				return o;
			}
		});
		
		Car obj=(Car)en.create();
		obj.move();
		
	}
}
