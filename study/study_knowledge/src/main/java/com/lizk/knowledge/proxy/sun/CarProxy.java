package com.lizk.knowledge.proxy.sun;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
/**
 * <B>描述:</B>
 * 代理的实现类
 * @author lizk
 * @date 2017年3月9日 14:04:39 
 * @version 1.0
 */
public class CarProxy implements InvocationHandler{

	private Object target;
	
	public Move bind(Object target){
		this.target = target;
		return (Move)Proxy.newProxyInstance(target.getClass().getClassLoader(), 
										target.getClass().getInterfaces(), this);
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println(proxy.getClass().getInterfaces());
		System.out.println(System.getProperty("user.dir")+"============");
		Object object = method.invoke(target, args);
		System.out.println("============");
		System.out.println(this.getClass().getClassLoader().getResource("/"));
		System.out.println(this.getClass().getClassLoader().getResource(""));
		System.out.println(this.getClass().getResource("/").getPath());
		System.out.println(this.getClass().getResource("").getPath());
		
		return object;
	}

}
