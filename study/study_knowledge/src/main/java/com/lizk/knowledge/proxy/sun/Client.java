package com.lizk.knowledge.proxy.sun;
/**
 * <B>描述:</B>
 * 测试类
 * @author lizk
 * @date 2017年3月9日 14:04:54 
 * @version 1.0
 */
public class Client {
	public static void main(String[] args) {
		Move object = (Move) new CarProxy().bind(new Car());
		object.up();
	}
}
