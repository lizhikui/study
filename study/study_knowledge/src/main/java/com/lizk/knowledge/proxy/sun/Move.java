package com.lizk.knowledge.proxy.sun;
/**
 * <B>描述:</B>
 * 被代理类实现的接口
 * @author lizk
 * @date 2017年3月9日 14:05:17 
 * @version 1.0
 */
public interface Move {
	public void move();
	public void up();
}
