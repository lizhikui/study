package com.lizk.knowledge.shutdown_hook;

import java.io.IOException;

public class Client {
	public static void main(String[] args) {
		MyShutdownHook myShutdownHook = new MyShutdownHook();
		Runtime.getRuntime().addShutdownHook(myShutdownHook);
		System.out.println("按任意键退出");
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
