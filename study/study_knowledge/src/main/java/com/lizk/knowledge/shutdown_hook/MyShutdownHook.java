package com.lizk.knowledge.shutdown_hook;

import java.io.IOException;

/**
 * Java虚拟机退出的钩子
 * 就是一个普通的线程类，把该类的示例加入到Runtime.getRuntime().addShutdownHook中，就可以在Java虚拟机退出时自动执行这个线程。
 * @author lizk
 *
 */
public class MyShutdownHook extends Thread{

	@Override
	public void run() {
		
		
		System.out.print("五秒后退出系统...");
		System.out.print("\b");
		
		try {
			sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
}
