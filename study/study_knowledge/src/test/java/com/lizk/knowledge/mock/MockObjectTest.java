package com.lizk.knowledge.mock;

import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lizhikui
 * @date 2020/2/25 17:08
 */
public class MockObjectTest {

    private static final Logger logger = LoggerFactory.getLogger(MockObjectTest.class);

    @Test
    public void test2(){

    }

    @Test
    public void test() {

        /*
         * mock的对象,在默认情况下,屏蔽了被mock对象的方法逻辑
         * 使用mockito打桩以后,返回指定的对象
         */
        MockObject mock = Mockito.mock(MockObject.class);

        //这种打桩方式,会先调用被mock对象的方法,然后设置调用方法的返回值
        Mockito.when(mock.getAge()).thenReturn(11);

        //这种打桩方式,不会执行被mock对象的方法,直接设置设置方法的返回值
        Mockito.doReturn("hahaha").when(mock).getName();


        /*
         * 可以设置不同的参数返回不同的值
         * 如果传入的参数不在设置的范围内,
         * 1.mock的对象会返回空
         * 2.spy的对象会执行被mock对象的方法的逻辑,并返回执行结果
         * 3.可以通过doCallRealMethod设置执行被mock对象的真实逻辑
         */
        Mockito.doReturn("A").when(mock).say("a");
        Mockito.doReturn("B").when(mock).say("b");
        Mockito.doReturn("C").doCallRealMethod().when(mock).say("c");


        logger.info("mock.getAge():{}",mock.getAge());
        logger.info("mock.getName():{}",mock.getName());

        logger.info("mock.say(\"a\"):{}",mock.say("a"));
        logger.info("mock.say(\"b\"):{}",mock.say("b"));
        logger.info("mock.say(\"c\"):{}",mock.say("c"));
        logger.info("mock.say(\"c\"):{}",mock.say("c"));
        logger.info("mock.say(\"d\"):{}",mock.say("d"));


        logger.info("------------------");

        /*
         * spy的对象,在默认情况下,调用它的方法会执行原对象的方法.
         * 使用mockito打桩以后,返回指定的对象
         */
        MockObject spy = Mockito.spy(MockObject.class);

        Mockito.when(spy.getAge()).thenReturn(11);


        Mockito.doReturn("hahaha").when(spy).getName();

        /*
           同一个打桩的点可以设置多个返回值或执行逻辑
           1.执行次数超过设置的结果数的时候,返回最后一个设置的结果
           2.可以通过thenCallRealMethod设置执行被mock对象的真实逻辑
         */
        Mockito.when(spy.getPhone()).then(item -> "1").then(invocationOnMock -> "2").thenThrow(new RuntimeException("随碟附送")).thenCallRealMethod();


        /*
         * 可以设置不同的参数返回不同的值
         * 如果传入的参数不在设置的范围内,
         * 1.mock的对象会返回空
         * 2.spy的对象会执行被mock对象的方法的逻辑,并返回执行结果
         * 3.同一个参数可以设置多个放回值
         *  3.1返回结果按执行次数依次返回
         *  3.2执行次数超过设置的结果数的时候,返回最后一个设置的结果
         */
        Mockito.doReturn("A").when(spy).say("a");
        Mockito.doReturn("B").when(spy).say("b");
        Mockito.doReturn("C1").doReturn("C2").when(spy).say("c");


        logger.info("spy.getAge():{}",spy.getAge());
        logger.info("spy.getName():{}",spy.getName());
        logger.info("spy.getPhone():{}",spy.getPhone());
        logger.info("spy.getPhone():{}",spy.getPhone());
        try {
            logger.info("spy.getPhone():{}",spy.getPhone());
        }catch (Exception e){
            logger.info("spy.getPhone():{}",e.getMessage());
        }
        logger.info("spy.getPhone():{}",spy.getPhone());
        logger.info("spy.getPhone():{}",spy.getPhone());


        logger.info("spy.say(\"a\"):{}",spy.say("a"));

        logger.info("spy.say(\"b\"):{}",spy.say("b"));
        logger.info("spy.say(\"c\"):{}",spy.say("c"));
        logger.info("spy.say(\"c\"):{}",spy.say("c"));
        logger.info("spy.say(\"c\"):{}",spy.say("c"));
        logger.info("spy.say(\"d\"):{}",spy.say("d"));
    }
}
