package com.lizk.tools.hotdeploy;

public class AutoExeClassUtil {
	
	
	private static AutoExeClassUtil autoExeClassUtil = null;
	private static Object object = new Object();
	private ClassLoadUtil classLoadUtil = null;
	
	//单例模式
	public static AutoExeClassUtil getInstance(String path){
		synchronized (object) {
			if(autoExeClassUtil == null){
				autoExeClassUtil = new AutoExeClassUtil(path); 
			}
		}
		return autoExeClassUtil;
	}
	
	private AutoExeClassUtil() {
	}
	private AutoExeClassUtil(String path) {
		this.classLoadUtil = new ClassLoadUtil(path);
	}
	
	
	public Object exeClazz(String ClazzName ,String methodName ,Object[] objects){
		Object result = null;
		result = classLoadUtil.exeClazz(ClazzName, methodName, objects);
		return result;
	}
	
}
