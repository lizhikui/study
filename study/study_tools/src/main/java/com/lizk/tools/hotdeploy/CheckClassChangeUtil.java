package com.lizk.tools.hotdeploy;

import java.io.File;
import java.util.HashMap;

public class CheckClassChangeUtil  {

	HashMap<String, Long> fileMap = new HashMap<>();
	boolean isChange = false;
	File file = null;

	public CheckClassChangeUtil(String path) {
		file = new File(path);
	}

	public boolean checkAllFile(){
		return checkAllFile(file);
	}
	
	public boolean checkAllFile(File file){
		//如果是目录，那么循环检查每个目录里的文件
		if(file.isDirectory()){
			File[] files = file.listFiles();
			for(File tmpFile :files){
				checkAllFile(tmpFile);
			}
		//如果是文件，那么比较文件最后修改日期和fileMap中存储的日期是否一直
		}else if(file.isFile()){
			long lastModified = file.lastModified();
			String fileName = file.getAbsolutePath()+file.getName();
			Long tmpLastModified = fileMap.get(fileName);
			
			//如果不一致，那么把新的修改日期记录到fileMap中，并修改isChange为true
			if(tmpLastModified==null||tmpLastModified!=lastModified){
				fileMap.put(fileName, lastModified);
				isChange = true;
			}
		}
		
		return isChange;
	}
	
	public void reSetIsChange(){
		isChange = false;
	}
	
}
