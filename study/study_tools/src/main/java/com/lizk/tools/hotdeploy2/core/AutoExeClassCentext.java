package com.lizk.tools.hotdeploy2.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 
 * <p>
 * 描述：用来存储需要使用的对象
 * </p>
 * 
 * @author lizk
 *
 */
public class AutoExeClassCentext {
	//---------内部使用的变量-------------
	URL url = null;
	String packageName = null;
	ClassLoader parentLoader = null;
	//--------------------------------
	
	
	
	//---------作为载体，暂存的对象-------------
	ChildClassLoader childClassLoader = null;	//用来存放自定义的类加载器
	CheckFileTask checkFileTask = null;			//检查文件变化的定时器
	CheckClassChange checkClassChange = null;	//具体检查文件变化的工具
	//--------------------------------
	
	/**
	 * 构造方法，根据参数初始化对象
	 * @param path			需要扫描的路径
	 * @param packageName	包名
	 * @param parentLoader	父级类加载器
	 * @param init			定时器第一次执行等待时间
	 * @param delay			定时器延时时间
	 */
	public AutoExeClassCentext(String path, String packageName, ClassLoader parentLoader, int init, int delay) {
		try {
			url = new File(path).toURI().toURL();
		} catch (MalformedURLException e) {
			System.err.println("文件路径输入错误");
			throw new RuntimeException("文件路径输入错误");
		}
		this.packageName = packageName;
		this.parentLoader = parentLoader;
		checkClassChange = new CheckClassChange(path);

		checkFileTask = new CheckFileTask(this, init, delay);
	}
	

	public void loadAllClass() {
		childClassLoader = new ChildClassLoader(new URL[] { url }, packageName, parentLoader);
	}

	public ChildClassLoader getChildClassLoader() {
		return childClassLoader;
	}

	public void setChildClassLoader(ChildClassLoader childClassLoader) {
		this.childClassLoader = childClassLoader;
	}

	public CheckFileTask getCheckFileTask() {
		return checkFileTask;
	}

	public void setCheckFileTask(CheckFileTask checkFileTask) {
		this.checkFileTask = checkFileTask;
	}

	public CheckClassChange getCheckClassChange() {
		return checkClassChange;
	}

	public void setCheckClassChange(CheckClassChange checkClassChange) {
		this.checkClassChange = checkClassChange;
	}
	


}
