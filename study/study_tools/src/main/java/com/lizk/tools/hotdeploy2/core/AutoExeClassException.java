package com.lizk.tools.hotdeploy2.core;

public class AutoExeClassException extends RuntimeException{

	public AutoExeClassException(String message) {
		super(message);
	}
	
	public AutoExeClassException(String message,Throwable cause){
		super(message,cause);
	}
	
	public AutoExeClassException(){
		
	}

	private static final long serialVersionUID = 4247737068754384336L;

}
