package com.lizk.tools.hotdeploy2.core;


import java.io.File;
import java.util.HashMap;



/**
 * 
 * <p>描述：检查class文件是否变化工具</p>
 * @author lizk
 *
 */
public class CheckClassChange  {

	HashMap<String, Long> fileMap = null;	//暂存文件的最后修改日期
	boolean isChange = false;				//标记是否有文件修改
	File file = null;						//扫描的目录

	public CheckClassChange(String path) {
		file = new File(path);
		fileMap = new HashMap<String, Long>();
	}

	public boolean checkAllFile() {
		if (!file.exists()) {
			throw new AutoExeClassException("待扫描目录不存在");
		}
		return checkAllFile(file);
	}
	
	/**
	 * 检查file这么目录下的所有文件是否有被修改过
	 * @param file
	 * @return
	 */
	public boolean checkAllFile(File file) {
		// 如果是目录，那么循环检查每个目录里的文件
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File tmpFile : files) {
				checkAllFile(tmpFile);
			}
			// 如果是文件，那么比较文件最后修改日期和fileMap中存储的日期是否一致
		} else if (file.isFile()) {
			long lastModified = file.lastModified();
			String fileName = file.getAbsolutePath() + file.getName();
			Long tmpLastModified = fileMap.get(fileName);

			// 如果不一致，那么把新的修改日期记录到fileMap中，并修改isChange为true
			if (tmpLastModified == null || tmpLastModified != lastModified) {
				fileMap.put(fileName, lastModified);
				isChange = true;
			}
		}

		return isChange;
	}
	
	
	/**
	 * 恢复是否修改标志位为未修改
	 */
	public void reSetIsChange(){
		isChange = false;
	}
	
	
}
