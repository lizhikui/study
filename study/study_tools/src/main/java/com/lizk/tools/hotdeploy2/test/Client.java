package com.lizk.tools.hotdeploy2.test;

import java.util.concurrent.TimeUnit;

import com.lizk.tools.hotdeploy2.core.AutoExeClassUtil;

public class Client {
	public static void main(String[] args) throws InterruptedException {
		
		//配置参数
		AutoExeClassUtil.config("E:/sts_workspace/test/bin","my.test",Client.class.getClassLoader(),0,5);
		//获取工具类实例
		AutoExeClassUtil a = AutoExeClassUtil.getInstance();
		//启动
		a.start();
		
		while(true){
			//执行指定类的方法
			a.exeClazz("my.test.Test", "test",null, null);
			TimeUnit.SECONDS.sleep(3);
		}
		
	}
}
