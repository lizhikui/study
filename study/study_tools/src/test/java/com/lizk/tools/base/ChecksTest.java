package com.lizk.tools.base;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * @author lizhikui
 * @date 2020/2/21 12:44
 */
public class ChecksTest {
    @Test
    public void test(){
        Random r = new Random();
        int i = r.nextInt(10);
        System.out.println(i);
        Checks.checkArgument( i < 4,() -> new RuntimeException("随机数不能大于4"));

        Checks.checkState(i < 2,() -> new RuntimeException("随机数不能大于2"));


        int count = r.nextInt(10);
        List<Integer> list = Lists.newArrayList();
        for (int j = 0; j <count; j++) {
            list.add(j);
        }
        System.out.println(list.size());
        Checks.checkElementIndex(6,list.size(),()->new RuntimeException("超过索引位置"));

        Checks.checkPositionIndex(6,list.size(),()->new RuntimeException("超过索引位置"));
    }
}
